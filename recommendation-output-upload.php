<?php

//queries mysql database with records from chatlog csv file and creates intent recommendations file to upload to Watson Assistant

require_once( 'watsonscraper.php' );
require( "functions.php" );

date_default_timezone_set( 'America/Detroit' );
$date = date( "Y-m-d H:i:s" );

//echo "start time: {$date}<br>";

header( 'Content-Type: text/csv' );
header( 'Content-Disposition: attachment; filename=recommendation_upload.csv"' );

$limit = 1000000;

mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsLogInputs = "SELECT distinct(input) FROM logs WHERE input IS NOT NULL LIMIT {$limit}";
$rsLogInputs = mysql_query( $query_rsLogInputs, $watsonscraper )or die( mysql_error() );
$row_rsLogInputs = mysql_fetch_assoc( $rsLogInputs );
$totalRows_rsLogInputs = mysql_num_rows( $rsLogInputs );

$ind = 0;

do {

    //echo "line: {$row_rsLogInputs['input']}<br>";

    $user_CSV[ $ind ] = array( $row_rsLogInputs[ 'input' ] );

    $ind++;

} while ( $row_rsLogInputs = mysql_fetch_assoc( $rsLogInputs ) );


$fp = fopen( 'php://output', 'w' );

foreach ( $user_CSV as $line ) {

    fputcsv( $fp, $line, ',' );
}

fclose( $fp );

$date2 = date( "Y-m-d H:i:s" );

//echo "done creating file with {$limit} rows<br>";
//echo "end time: {$date2}";
?>