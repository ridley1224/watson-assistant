<?php

require_once( 'watsonscraper.php' );
include( "functions.php" );


$colname_rsWorkspaceDetails = "-1";
if ( isset( $_GET[ 'wid' ] ) ) {
  $colname_rsWorkspaceDetails = $_GET[ 'wid' ];
}

mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsWorkspaceDetails = sprintf( "SELECT * FROM workspaces WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsWorkspaceDetails = mysql_query( $query_rsWorkspaceDetails, $watsonscraper )or die( mysql_error() );
$row_rsWorkspaceDetails = mysql_fetch_assoc( $rsWorkspaceDetails );
$totalRows_rsWorkspaceDetails = mysql_num_rows( $rsWorkspaceDetails );


$colname_rsWorkspaces = "-1";
if ( isset( $_SESSION[ 'uid' ] ) ) {
  $colname_rsWorkspaces = $_SESSION[ 'uid' ];
}

$query_rsLogs = sprintf( "SELECT count(logid) as 'count' FROM logs WHERE userid = %s AND workspaceid = %s LIMIT 1", GetSQLValueString( $colname_rsWorkspaces, "int" ), GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsLogs = mysql_query( $query_rsLogs, $watsonscraper )or die( mysql_error() );
$row_rsLogs = mysql_fetch_assoc( $rsLogs );
$totalRows_rsLogs = mysql_num_rows( $rsLogs );

//echo $query_rsLogs;

?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Intent Recommendation File - <?php echo $row_rsWorkspaceDetails['workspacename']; ?></title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Intent Recommendation File</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p id="workspaceLbl"><?php echo $row_rsWorkspaceDetails['workspacename']; ?></p>
    <div id="renderContent">
      <?php
      if ( $row_rsLogs[ 'count' ] > 0 ) {
        ?>
      <p>Generate your Watson Assistant Intent Recommendations file to upload to Watson Assistant.</p>
      <p id="createFileDiv"><a href="recommendation-output-upload.php">Create Watson CSV file</a></p>
      <?php
} else {?>
      <p>You currently don't have chat logs. To upload logs, click <a href="parse-logs-file.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">here</a>.</p>
      <?php } ?>
    </div>
    <p id="docContentDiv"> </p>
    <div class="backDiv"><a href="workspace-details.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Back</a></div>
  </div>
  <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>
<?php
mysql_free_result($rsWorkspaces);
?>