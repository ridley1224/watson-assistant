<?php

require("functions.php");

$debug = false;
$outputFile = true;

if(isset($_POST["submit"])) {

  $target_file = basename($_FILES["fileToUpload"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  $imageFileType = "csv";

  if($imageFileType == "csv")
  {
    if($outputFile == true)
    {
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="new_intents_automation_output.csv"');
    }

    $csvFile = $_FILES["fileToUpload"]["tmp_name"]; //'new-intents.csv';

    //$csvFile = 'automate1.csv';
    $utteranceList = readCSV($csvFile);

    if($outputFile == true)
    {
        $user_CSV[0] = array('','Trained Utterance', 'Intent', 'Confidence');
    }

    $i = 0;
    //$limit = $_POST["limit"];
    $limit = $_POST["limit"];

    foreach ($utteranceList as $rowString)
    {
      if ($i == 0)
      {
        $obj1 = new stdClass;
        $intent = $rowString;
        $obj1->intent = $intent;

        if($debug == true)
        {
          echo "<strong>Intent:</strong> {$rowString} ";
        }

        $i++;
      }
      else if ($i == 1)
      {
        $utterance = $rowString;
        $obj1->utterance = $utterance;

        if($debug == true)
        {
          echo "<strong>Utterance:</strong> {$rowString} ";
        }

        $i++;
      }
      else
      {
        if($debug == true)
        {
          echo "<strong>Confidence:</strong> {$rowString}<br>";
        }

        $confidence = $rowString;
        $obj1->confidence = $confidence;
        if($confidence > 0.89)
        {
          $csvList[] = $obj1;
        }

        $i = 0;
      }

    } //end loop

    if($debug == true)
    {
      echo '<br>CSV List > 90% Confidence<pre>';
      print_r($csvList);
      echo '</pre>';
    }

    //ar_dump($examplesList);

    //get individual utterances

    $lastIntent = "";
    $i = 0;

    foreach ($csvList as $example)
    {
      $intent = $example->intent;

      if($i==0)
      {
        $intentList[] = $intent;
      }
      else
      {
        if($intent != $lastIntent)
        {
          $csvIntentList[] = $intent;
          $lastIntent = $intent;
        }
        else {}
      }

      $i++;
    }

    //var_dump($csvIntentList);

    //get intent examples

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/c208a7e6-09f0-485f-94ed-33434c0a7f4a/intents?version=2018-09-20&export=true",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
        "Postman-Token: 238616e5-3692-4553-8104-1e34087b2d5e",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {

      //echo "cURL Error #:" . $err;

    } else {

      //echo $response;

      $outputInd = 1;

      $decodedData = json_decode($response);

      $currentWatsonIntents = $decodedData->intents;

      //var_dump($currentWatsonIntents);

      if($debug == true)
      {
        echo "<p>Query Watson Intents</p>";
      }

      foreach ($currentWatsonIntents as $watsonOb)
      {
        $obj2 = new stdClass;

        if($debug == true)
        {
          echo "Watson Intent: " . $watsonOb->intent . "<br>";
        }

        $obj2->intent = $watsonOb->intent;
        $obj2->examples = [];

        $examples = $watsonOb->examples;

        //var_dump($examples);

        foreach ($examples as $example)
        {
          $obj2->examples[]  = "{\"text\": \"{$example->text}\"}";
          $allUtterances[] = $example->text;
        }

        foreach ($csvList as $csvUtteranceOb)
        {
          if($watsonOb->intent == $csvUtteranceOb->intent)
          {
            if($debug == true)
            {
              echo "  Trained utterance: " . $csvUtteranceOb->utterance . "<br>";
            }

            //var_dump($watsonOb);

            if (!in_array($csvUtteranceOb->utterance, $allUtterances)) {

              $obj2->examples[]  = "{\"text\": \"{$csvUtteranceOb->utterance}\"}";

              if($outputFile == true)
              {
                $user_CSV[$outputInd] = array("", $csvUtteranceOb->utterance, $watsonOb->intent, $csvUtteranceOb->confidence);
                $outputInd++;
              }
            }
          }
        }

        $newExamples[] = $obj2;
      }
    }

    //var_dump($newExamples);

    // echo '<pre>';
    // print_r($allUtterances);
    // echo '</pre>';

    if($debug == true)
    {
      echo '<br>Combined CSV and Updated Watson Examples<pre>';
      print_r($newExamples);
      echo '</pre>';
    }

    //start updates loop

    foreach ($newExamples as $example)
    {
      //echo "intent2: {$example->intent}<br>";

      $workspace = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/{$_POST['workspace']}/intents/{$example->intent}?version=2018-09-20";

      //echo "workspace: {$workspace}<br>";

      $postfields = "{\"intent\": \"{$example->intent}\", \"examples\": [".implode(",",$example->examples)."]}";

      //echo "postfields: {$postfields}<br>";

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $workspace,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $postfields, //, \"description\":\"insurance options\"}
        CURLOPT_HTTPHEADER => array(
          "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
          "Content-type: application/json",
          "Postman-Token: d5eddef6-3d38-4e47-a540-8f4fe13725b2",
          "cache-control: no-cache"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        //echo "cURL Error #:" . $err;
      } else {

        if($debug == true)
        {
          echo "Watson response: {$response}<br>";
        }

        $decodedData = json_decode($response);
      }

    } // end updates loop

    if($debug == true)
    {
      echo "<p>Done</p>";
    }

    $uploadOk = 1;

    if($outputFile == true)
    {
      $fp = fopen('php://output', 'w');

      foreach ($user_CSV as $line) {

          // though CSV stands for "comma separated value"
          // in many countries (including France) separator is ";"

          fputcsv($fp, $line, ',');
      }

      fclose($fp);
    }
  }
  else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
  }
}

if($uploadOk == 0) {

  // if(isset($_POST["submit"]) && !isset($_POST["fileToUpload"])) {
  //
  //   echo "<p>Please select CSV file</p>";
  // }
?>

<!DOCTYPE html>
<html>
<title>Train Intents</title>
<body>

<form action="auto-add-intents.php" method="post" enctype="multipart/form-data">
    <table cellspacing="5">
      <tr><td>Workspace ID</td><td><input type="text" name="workspace" id="workspace" value="c208a7e6-09f0-485f-94ed-33434c0a7f4a"></td></tr>
      <tr><td>File</td><td><input type="file" name="fileToUpload" id="fileToUpload"></td></tr>
      <!-- <tr><td>Processed Count</td><td id="processed">0</td></tr> -->
      <tr><td></td><td><input type="submit" value="Upload CSV" name="submit"></td></tr>
  </table>
</form>

</body>
</html>

<?php } ?>
