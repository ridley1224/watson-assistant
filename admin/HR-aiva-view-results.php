<?php

//loops through utterances csv file and checks for first_intent_match variable from watson. if variable is present, column in created csv file is filled with 'yes'

//phpinfo();

require("functions.php");

$debug = false;
$showOutput = true;
$createFile = false;

if (isset($_POST["submit"])) {

  $target_file = basename($_FILES["fileToUpload"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

  if ($imageFileType == "csv") {

    //$csvFile = 'userlogs.csv';

    //var_dump($csvFile);

    $csvFile = $_FILES["fileToUpload"]["tmp_name"];

    if ($createFile == true) {
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="hr_watson_result.csv"');
    }

    $utteranceList = readCSV($csvFile);

    $user_CSV = array('Question', 'Expected_Response', 'Received_Response', 'Match');

    $i = 0;
    $limit = $_POST["limit"];
    $counter = 0;
    //$debug = false;
    $rows = 0;

    //echo "start time: {$date}<br>";

    foreach ($utteranceList as $utterance) {

      if ($rows < $limit) {

        if ($counter % 4 == 0) {
          if ($obj1) {
            //echo "reset<br>";
            $i = 0;
            $csvList[] = $obj1;
            $obj1 = null;
          }

          //echo "new {$rows}<br>";
          $obj1 = new stdClass;
          $obj1->$user_CSV[$i] = strip_tags($utterance);
          $rows++;
        } else {
          $obj1->$user_CSV[$i] = strip_tags($utterance);
        }

        //echo "row {$i} counter {$counter}: {$utterance}<br>";

        $i++;
      } // end limit condition

      $counter++;
    } //end loop

    if ($debug == true) {
      print "<pre>";
      print_r($csvList);
      print "</pre>";
    }

    if ($debug == true) {
      print "<pre>";
      print_r($user_CSV);
      print "</pre>";
    }

    //array_unshift($user_CSV,array( 'Question', 'Expected_Response', 'Received_Response', 'Match', 'link1', 'link2', 'link3' ));


    //watson assistant API

    $workspace = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/{$_POST['workspace']}/message?version=2017-05-26";

    foreach ($csvList as $obj) {
      $utterance = $obj->Question;

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $workspace,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\"input\": {\"text\": \"{$utterance}\"}, \"context\":{\"car_type\":\"hr\"}}",
        CURLOPT_HTTPHEADER => array(
          "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
          "Content-type: application/json",
          "Postman-Token: 19bca2b6-e0ff-4bd3-8b44-225dbba5dc94",
          "cache-control: no-cache"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {

        echo "cURL Error #:" . $err;
      } else {

        $decodedData = json_decode($response);

        //   if($debug == true)
        //   {
        //     echo "<pre>";
        //     print_r($decodedData);
        //     echo "</pre>";
        //   }

        //check for disambiguation

        if ($decodedData->context->first_intent_match) {
          $disambiguate = "yes";
          $outputString = "Would you like " . $decodedData->context->first_intent_match . " or &" . $decodedData->context->second_intent_match . "?";
        } else {
          $disambiguate = "no";
          $outputString = $decodedData->output->text[0];
        }

        $strippedResponse = htmlspecialchars_decode(strip_tags($outputString));
        $strippedResponse = str_replace('"', '', $strippedResponse);
        //$strippedResponse = html_entity_decode($strippedResponse);

      
        $strippedResponse = str_replace('&nbsp;', ' ', $strippedResponse);
        //$strippedResponse = str_replace('Please click here for additional information.', '', $strippedResponse);

        $strippedExpected = htmlspecialchars_decode(strip_tags($obj->Expected_Response));
        $strippedExpected = str_replace('"', '', $strippedExpected);
        

        if (trim($strippedResponse) == trim($strippedExpected)) {
          $match = "yes";
        } else {
          $match = "no";
        }

        $ind = $i + 1;

        $user_CSV[$ind] = array($utterance, $strippedExpected, $strippedResponse, $match);

        if ($showOutput == true) {
          echo " <strong>Question:</strong> " .  $utterance . "<br><strong>Expected Response:</strong> " . $strippedExpected . "<br><strong>Watson Response:</strong> " . $strippedResponse . "<br><strong>Match?:</strong> " . $match . "<br><br>";
        }
      }

      //$user_CSV[0] = array( 'Question', 'Expected_Response', 'Received_Response', 'Match', 'link1', 'link2', 'link3' );

      if ($createFile == true) {
        $fp = fopen('php://output', 'w');

        foreach ($user_CSV as $line) {

          fputcsv($fp, $line, ',');
        }

        fclose($fp);
      }
    }

    if ($debug == true) {
      print "<pre>";
      print_r($user_CSV);
      print "</pre>";
    }

    $uploadOk = 1;
  } else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
  }
}

if ($uploadOk == 0) {

  // if(isset($_POST["submit"]) && !isset($_POST["fileToUpload"])) {
  //
  //   echo "<p>Please select CSV file</p>";
  // }
  ?>

  <!DOCTYPE html>
  <html>

  <head>
    <title>HR AIVA</title>
  </head>

  <body>

  <table cellspacing="10"><tr><td><a href="HR-aiva-view-results.php">View Results</a></td><td><a href="HR-aiva-create-file.php">Create CSV</a></td></tr></table>

    <form action="HR-aiva-view-results.php" method="post" enctype="multipart/form-data" title="Auto Disambiguate">
      <table cellspacing="5">
        <tr>
          <td>Workspace ID</td>
          <td><input type="text" name="workspace" id="workspace" value="f8113cf4-f42a-4846-a478-d3cf8e845967"></td>
        </tr>
        <tr>
          <td>Records to Process</td>
          <td><input type="text" name="limit" id="limit" value="10"></td>
        </tr>
        <tr>
          <td>File</td>
          <td><input type="file" name="fileToUpload" id="fileToUpload"></td>
        </tr>
        <!-- <tr><td>Processed Count</td><td id="processed">0</td></tr> -->
        <tr>
          <td></td>
          <td><input type="submit" value="Upload CSV" name="submit"></td>
        </tr>
      </table>
    </form>

  </body>

  </html>

<?php } ?>