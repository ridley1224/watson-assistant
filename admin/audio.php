<?php

$curl = curl_init();

$message .= "ayva, or the “Artificial Intelligence Virtual Assistant” product, provides markets the ability to provide an alternative to a live service agent that can perform a variety of customer specific self-service functions.";
$message .= " Consistent with our vehicle experience, the virtual assistant branded “Mercedes” is deployable in a poortal or app, can understand many languages spoken in our markets, and uses data from your local systems to replicate many of the functions performed by ay call center.";
$message .= " With Mercedes, self-service is only a chat away. On the roadmap for the product’s evolution is expansion to service voice-based phone calls, ability to transfer to service agent chat, integration with home digital assistants, and even accessibility to financial services through our cars’ MBUX.";
$message .= " How can Mercedes help your customers today?";
$message .= " This message is brought to you by IBM text to speech.";


//$voice = "US_AllisonVoice";
$voice = "en-US_LisaV3Voice";
$format = urlencode("audio/mp3");
$messageP = urlencode($message);

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://stream.watsonplatform.net/text-to-speech/api/v1/synthesize?voice={$voice}&accept={$format}&text={$messageP}",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic YXBpa2V5Ok9WVE9DZTlLZDNkQXI2cWF4MXZ1aFV1V2g0UHF6RWUtV1ZTZ291dlNyR29D",
    "cache-control: no-cache",
    "postman-token: 9782a696-ad74-ac9b-8c1b-e60981da9f87"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

    //echo $response;

    $filename = "output/";
    //$filename .= 'audio' . time() . '.wav';
    $filename .= "aiva-audio.mp3";

    $file = fopen($filename, 'w');
    fwrite($file, $response);
    fclose($file);
    echo $filename;
}

?>