<?php

//loops through utterances csv file and checks for first_intent_match variable from watson. if variable is present, column in created csv file is filled with 'yes'


require("functions.php");

$debug = true;

if(isset($_POST["submit"])) {

  // if($debug == true)
  // {
  //   $imageFileType = "csv";
  // }
  // else
  // {
  //   $target_file = basename($_FILES["fileToUpload"]["name"]);
  //   $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  // }

  $target_file = basename($_FILES["fileToUpload"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  if($imageFileType == "csv")
  {
    if($debug == false)
    {
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="disambiguation_test_result.csv"');

    }
    // else
    // {
    //   $csvFile = 'disambiguation-test.csv';
    // }

    // header('Content-Type: text/csv');
    // header('Content-Disposition: attachment; filename="disambiguation_test_result.csv"');

    $csvFile = $_FILES["fileToUpload"]["tmp_name"]; //'disambiguation-test.csv';

    $utteranceList = readCSV($csvFile);

    $user_CSV[0] = array('','Utterance', 'Intent', 'Response', 'Matches?');

    // very simple to increment with i++ if looping through a database result

    $i = 0;
    $limit = $_POST["limit"];

    
    $workspace = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/{$_POST['workspace']}/message?version=2017-05-26";
    //$workspace = "https://gateway.watsonplatform.net/conversation/api/v1/workspaces/{$_POST['workspace']}/message?version=2017-05-26";

    foreach ($utteranceList as $utterance)
    {
      if ($i < $limit)
      {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $workspace,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\"input\": {\"text\": \"{$utterance}\"},\"alternate_intents\": true}",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
            "Content-type: application/json",
            "Postman-Token: 3d0c57eb-2e60-4cd2-80c3-db721d2c0fb4",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

          echo "cURL Error #:" . $err;

        } else {

          $decodedData = json_decode($response);

          // if($debug == true)
          // {
          //   echo "<pre>";
          //   print_r($decodedData);
          //   echo "</pre>";
          // }

          //check for disambiguation

          if($decodedData->context->first_intent_match)
          {
            $disambiguate = "yes";
            $outputString = "Would you like ".$decodedData->context->first_intent_match." or &".$decodedData->context->second_intent_match."?";
          }
          else
          {
            $disambiguate = "no";
            $outputString = $decodedData->output->text[0];
          }

          //populate row

          $ind = $i + 1;

          $user_CSV[$ind] = array($ind, $utterance, $decodedData->intents[0]->intent, $outputString, $disambiguate);

          if($debug == true)
          {
            echo " <strong>Question:</strong> " .  $utterance . "<br><strong>Expected Response:</strong> " . $decodedData->intents[0]->intent . "<br><strong>Response:</strong> " . $outputString . "<br><strong>Match?:</strong> " . $disambiguate . "<br><br>";
          }
        }

        $i++; // increment limit counter

      } // end limit condition

    } //end loop


      if($debug == false)
      {
        $fp = fopen('php://output', 'w');

        foreach ($user_CSV as $line) {

            // though CSV stands for "comma separated value"
            // in many countries (including France) separator is ";"

            fputcsv($fp, $line, ',');
        }

        fclose($fp);
    }

    $uploadOk = 1;
  }
  else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
  }
}

if($uploadOk == 0) {

  // if(isset($_POST["submit"]) && !isset($_POST["fileToUpload"])) {
  //
  //   echo "<p>Please select CSV file</p>";
  // }
?>

<!DOCTYPE html>
<html>
<head><script src="jquery.js"></script>
<title>Disambiguation Test</title>
</head>
<body>

<form action="auto-disambiguate.php" method="post" enctype="multipart/form-data" title="Auto Disambiguate">
    <table cellspacing="5">
      <tr><td>Workspace ID</td><td><input type="text" name="workspace" id="workspace" value="b857c945-0dea-44cb-9e0b-67d140a1bfc0"></td></tr>
      <tr><td>Records</td><td><input type="text" name="limit" id="limit" value="5"></td></tr>
      <tr><td>File</td><td><input type="file" name="fileToUpload" id="fileToUpload"></td></tr>
      <!-- <tr><td>Processed Count</td><td id="processed">0</td></tr> -->
      <tr><td></td><td><input type="submit" value="Upload CSV" name="submit"></td></tr>
  </table>
</form>

</body>
</html>

<?php } ?>
