<?php

//loops through utterances csv file and checks for first_intent_match variable from watson. if variable is present, column in created csv file is filled with 'yes'

require("functions.php");

if(isset($_POST['output']))
{
  $debug = false;
}
else
{
  $debug = true;
}

if(isset($_POST["submit"])) {

  $target_file = basename($_FILES["fileToUpload"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  if($imageFileType == "csv")
  {
    if($debug == false)
    {
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="intents_result.csv"');
    }

    $csvFile = $_FILES["fileToUpload"]["tmp_name"]; //'disambiguation-test.csv';

    $utteranceList = readCSV($csvFile);

    $user_CSV[0] = array('Utterance', 'Intent');

    // very simple to increment with i++ if looping through a database result

    $i = 0;
    $limit = $_POST["limit"];

    $workspace = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/{$_POST['workspace']}/message?version=2017-05-26";

    foreach ($utteranceList as $utterance)
    {
      if ($i < $limit)
      {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $workspace,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\"input\": {\"text\": \"{$utterance}\"},\"alternate_intents\": true}",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic YXBpa2V5Ok1Bc3EyMVN5el9adVNWd3JNYlFRZGQxSFRPTDZOVXN1ZFRoZEFKa1llc2hs",
            "Content-type: application/json",
            "Postman-Token: 69150048-dc32-4378-ab65-df68695833f8",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

          echo "cURL Error #:" . $err;

        } else {

          $decodedData = json_decode($response);

          //populate row

          $ind = $i + 1;

          $user_CSV[$ind] = array($utterance, $decodedData->intents[0]->intent);

          if($debug == true)
          {
            echo " <strong>Utterance:</strong> " .  $utterance . "<br><strong>Intent Result:</strong> " . $decodedData->intents[0]->intent . "<br><br>";
          }
        }

        $i++; // increment limit counter

      } // end limit condition

    } //end loop

      if($debug == false)
      {
        $fp = fopen('php://output', 'w');

        foreach ($user_CSV as $line) {

            fputcsv($fp, $line, ',');
        }

        fclose($fp);
    }

    $uploadOk = 1;
  }
  else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
  }
}

if($uploadOk == 0) {

  // if(isset($_POST["submit"]) && !isset($_POST["fileToUpload"])) {
  //
  //   echo "<p>Please select CSV file</p>";
  // }
?>

<!DOCTYPE html>
<html>
<head><script src="jquery.js"></script>
<title>Intent Test</title>
</head>
<body>

<form action="test-utterances.php" method="post" enctype="multipart/form-data" title="Auto Disambiguate">
    <table cellspacing="5">
      <tr><td>Workspace ID</td><td><input type="text" name="workspace" id="workspace" value="a1b3eff6-6259-4b0d-ae79-873805f4aa44"></td></tr>
      <tr><td>Records</td><td><input type="text" name="limit" id="limit" value="100"></td></tr>
      <tr><td>File</td><td><input type="file" name="fileToUpload" id="fileToUpload"></td></tr>
      <tr><td>Output to file?</td><td><input type="checkbox" name="output" id="output"></td></tr>
      <!-- <tr><td>Processed Count</td><td id="processed">0</td></tr> -->
      <tr><td></td><td><input type="submit" value="Upload CSV" name="submit"></td></tr>
  </table>
</form>

</body>
</html>

<?php } ?>