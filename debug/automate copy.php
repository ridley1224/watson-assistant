<?php

require("functions.php");

//if(0==0) {
if(isset($_POST["submit"])) {

  $target_file = basename($_FILES["fileToUpload"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  $imageFileType = "csv";

  if($imageFileType == "csv")
  {
    // header('Content-Type: text/csv');
    // header('Content-Disposition: attachment; filename="disambiguation_test.csv"');

    $csvFile = $_FILES["fileToUpload"]["tmp_name"]; //'utterances.csv';

    //$csvFile = 'automate1.csv';
    $utteranceList = readCSV($csvFile);

    //$user_CSV[0] = array('','Utterance', 'Intent', 'Disambiguate?', 'Response');

    // very simple to increment with i++ if looping through a database result

    $i = 0;
    //$limit = $_POST["limit"];
    $limit = $_POST["limit"];

    foreach ($utteranceList as $rowString)
    {
      // if ($i < $limit)
      // {
        if ($i %2 == 0)
        {
          $obj1 = new stdClass;
          $intent = $rowString;
          $obj1->intent = $intent;

          //echo "intent: {$intent}<br>";
        }
        else {

          $utterance = $rowString;
          $obj1->utterance = $utterance;

          $cvsList[] = $obj1;

          //echo "utterance: {$utterance}<br>";
        }

        $i++; // increment limit counter

      //} // end limit condition

    } //end loop

    //ar_dump($examplesList);

    //get individual utterances

    $lastIntent = "";
    $i = 0;

    foreach ($cvsList as $example)
    {
      $intent = $example->intent;

      if($i==0)
      {
        $intentList[] = $intent;
      }
      else {

        if($intent != $lastIntent)
        {
          $csvIntentList[] = $intent;
          $lastIntent = $intent;
        }
        else {

        }
      }

      $i++;
    }

    //var_dump($csvIntentList);

    // foreach ($csvIntentList as $intent)
    // {
    //
    // }

    //get intent examples

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/c208a7e6-09f0-485f-94ed-33434c0a7f4a/intents?version=2018-09-20&export=true",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
        "Postman-Token: 238616e5-3692-4553-8104-1e34087b2d5e",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;

      $decodedData = json_decode($response);

      $currentWatsonIntents = $decodedData->intents;

      //var_dump($currentWatsonIntents);

      foreach ($currentWatsonIntents as $watsonOb)
      {
        $obj2 = new stdClass;

        echo "intent: " . $watsonOb->intent . "<br>";

        $obj2->intent = $watsonOb->intent;
        $obj2->examples = [];

        $examples = $watsonOb->examples;

        //var_dump($examples);

        foreach ($examples as $example)
        {
          $obj2->examples[]  = "{\"text\": \"{$example->text}";
        }

        foreach ($cvsList as $csvUtteranceOb)
        {
          if($watsonOb->intent == $csvUtteranceOb->intent)
          {
            echo "  example: " . $csvUtteranceOb->utterance . "<br>";

            //var_dump($watsonOb);

            $obj2->examples[]  = "{\"text\": \"{$csvUtteranceOb->utterance}";
          }
        }

        $newExamples[] = $obj2;
      }
    }

    //var_dump($newExamples);

    echo '<pre>';
    print_r($newExamples);
    echo '</pre>';


    foreach ($examplesList as $example)
    {
      //echo "run example<br>";

      echo "intent2: {$example->intent}<br>";
      echo "utterance2: {$example->utterance}<br>";

      $workspace = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/{$_POST['workspace']}/intents/{$example->intent}?version=2018-09-20";

      echo "workspace: {$workspace}<br>";

      $postfields = "{\"intent\": \"{$example->intent}\", \"examples\": [{\"text\":\"{$example->utterance}\"}]}";

      echo "postfields: {$postfields}<br>";

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $workspace,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $postfields, //, \"description\":\"insurance options\"}
        CURLOPT_HTTPHEADER => array(
          "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
          "Content-type: application/json",
          "Postman-Token: d5eddef6-3d38-4e47-a540-8f4fe13725b2",
          "cache-control: no-cache"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {

        echo "response: {$response}<br>";

        $decodedData = json_decode($response);
      }
    } // end examples loop

    echo "done";

    $uploadOk = 1;
  }
  else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
  }
}

if($uploadOk == 0) {

  // if(isset($_POST["submit"]) && !isset($_POST["fileToUpload"])) {
  //
  //   echo "<p>Please select CSV file</p>";
  // }
?>

<!DOCTYPE html>
<html>
<body>

<form action="automate.php" method="post" enctype="multipart/form-data">
    <table cellspacing="5">
      <tr><td>Workspace ID</td><td><input type="text" name="workspace" id="workspace" value="c208a7e6-09f0-485f-94ed-33434c0a7f4a"></td></tr>
      <tr><td>File</td><td><input type="file" name="fileToUpload" id="fileToUpload"></td></tr>
      <!-- <tr><td>Processed Count</td><td id="processed">0</td></tr> -->
      <tr><td></td><td><input type="submit" value="Upload CSV" name="submit"></td></tr>
  </table>
</form>

</body>
</html>

<?php } ?>
