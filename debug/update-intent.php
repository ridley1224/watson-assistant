<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/c208a7e6-09f0-485f-94ed-33434c0a7f4a/intents/First_Class_Protection?version=2018-09-20",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"intent\": \"First_Class_Protection\", \"examples\": [{\"text\":\"what does your protection include?\"}]}\n",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
    "Content-type: application/json",
    "Postman-Token: 73a33d89-d233-4346-8eb4-d7d7ff748d9e",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

?>
