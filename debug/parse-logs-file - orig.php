<?php
require_once( 'watsonscraper.php' );
include( "functions.php" );
?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Upload Logs</title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <div id="headerBG" class="clearfix"> <span style="font-size:30px;cursor:pointer"><img id="navIcon" name="navIcon" src="img/Hamburger_icon.png" class="image"/></span> <a href="intent-list.php"><img id="logo" src="chrome/icons/wa48.png" class="image"/></a> </div>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Upload Logs</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p>&nbsp;</p>
    <div id="renderContent"></div>
    <form method="post" action="parse-logs-file.php" enctype="multipart/form-data">
      <table  cellpadding="5" cellspacing="5">
        <tr>
          <td><input type="file" name="upload" id="upload" style="height: 35px; width: 200px"></td>
        </tr>
        <tr>
          <td><div id="nppi"><span>Strip NPPI</span>
              <input type="checkbox" value="stripNPPI" id="stripNPPI" name="submit">
              <img src="img/question.png" width="15" height="15"></div></td>
        </tr>
        <tr>
          <td><input type="submit" value="Submit" name="submit"></td>
        </tr>
      </table>
    </form>
    <p id="docContentDiv">
      <?php include("includes/parse-logs.php");?>
    </p>
  </div>
  <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>
<?php
mysql_free_result( $rsFiles );
?>