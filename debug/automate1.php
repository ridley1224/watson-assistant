<?php

$example = new stdClass;
$example->intent = "First_Class_Protection";
$example->utterance = "test utterance";

// echo "intent2: {$example->intent}<br>";
// echo "utterance2: {$example->utterance}<br>";

$postfields = "{\"intent\": \"First_Class_Protection\", \"examples\": [{\"text\":\"test utterance\"}]}";

echo "postfields: {$postfields}<br>";

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/c208a7e6-09f0-485f-94ed-33434c0a7f4a/intents/First_Class_Protection?version=2018-09-20",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $postfields, //, \"description\":\"insurance options\"}
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic YjVjM2Q3ZTUtY2NiOS00NzFhLTg5MGItOWI2OWE5MmY4ZDMzOk5LV3dvOHhkMVpDYg==,Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
    "Content-type: application/json",
    "Postman-Token: f5363837-46f3-4bfb-b8a7-11ab7e90555e",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

  echo "response: {$response}";

  $decodedData = json_decode($response);
}


?>
