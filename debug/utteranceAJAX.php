<?php

header('Content-type: application/xml');

require("functions.php");

$_GET["limit"] = 5;
$_GET['workspace'] = "7402c645-5560-4788-8daf-5eabc61856c3";

$target_file = basename($_FILES["fileToUpload"]["name"]);
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

$imageFileType = "csv";

if($imageFileType == "csv")
{
  //header('Content-Type: text/csv');
  //header('Content-Disposition: attachment; filename="disambiguation_test.csv"');

  $csvFile = 'utterances.csv';

  $utteranceList = readCSV($csvFile);

  $user_CSV[0] = array('','Utterance', 'Intent', 'Disambiguate?', 'Response');

  // very simple to increment with i++ if looping through a database result

  $i = 0;
  $limit = $_GET["limit"];

  $workspace = "https://gateway.watsonplatform.net/conversation/api/v1/workspaces/{$_GET['workspace']}/message?version=2017-05-26";

  foreach ($utteranceList as $utterance)
  {
    if ($i < $limit)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $workspace,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_GETFIELDS => "{\"input\": {\"text\": \"{$utterance}\"},\"alternate_intents\": true}",
        CURLOPT_HTTPHEADER => array(
          "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
          "Content-type: application/json",
          "GETman-Token: f5249426-5d62-4d01-bf7c-dcadfed09684",
          "cache-control: no-cache"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {

        //echo "cURL Error #:" . $err;

      } else {

        $decodedData = json_decode($response);

        //check for disambiguation

        if($decodedData->context->first_intent_match)
        {
          $disambiguate = "yes";
          $outputString = "Would you like ".$decodedData->context->first_intent_match." or &".$decodedData->context->second_intent_match."?";
        }
        else
        {
          $disambiguate = "no";
          $outputString = $decodedData->output->text[0];
        }

        //populate row

        $ind = $i + 1;

        $user_CSV[$ind] = array($ind, $utterance, $decodedData->intents[0]->intent, $disambiguate, $outputString);


        echo '<?xml version="1.0" encoding="ISO-8859-1"?><markers>';

            echo '<marker ';
            echo 'count="' . parseToXML(5) . '" ';
            echo '/>';

        echo '</markers>';
      }

      $i++; // increment limit counter




    } // end limit condition



  } //end loop

  $fp = fopen('php://output', 'w');

  foreach ($user_CSV as $line) {

      // though CSV stands for "comma separated value"
      // in many countries (including France) separator is ";"

      fputcsv($fp, $line, ',');
  }

  fclose($fp);

  $uploadOk = 1;
}
else {

  //echo "Invalid file type. Please choose CSV file";

  $uploadOk = 0;
}


?>
