<?php
require_once( 'watsonscraper.php' );
include( "functions.php" );
include( "en-de.php" );

if(isset($_GET['uid']))
{
    $_SESSION['uid'] = de($_GET['uid']);

    $query_rsLogs = "SELECT * FROM users WHERE userid = {$_SESSION['uid']}";

    //echo "query: " . $query_rsLogs;

    $rsLogs = mysql_query( $query_rsLogs, $watsonscraper )or die( mysql_error() );
    $row_rsLogs = mysql_fetch_assoc( $rsLogs );
    $totalRows_rsLogs = mysql_num_rows( $rsLogs );

    $_SESSION[ 'MM_Username' ] = $row_rsLogs['email'];
}

?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>My Workspaces</title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">My Workspaces</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <div id="renderContent"></div>
    <form method="post" action="my-workspaces.php">
      <table  cellpadding="5" cellspacing="5">
        <?php if(isset($_POST['search'])){ $search = $_POST['search'];} else if(isset($_GET['search'])){ $search = $_GET['search'];} ?>
        <tr>
          <td><input type="text" name="search" placeholder="Search" class="inputBox" value="<?php echo $search;?>"></td>
        </tr>
        <tr>
          <td><input type="submit" value="Submit" name="submit" class="submitBtn"></td>
        </tr>
      </table>
</form>
    <p id="docContentDiv">
      <?php include("includes/workspacesHTML.php");?>
    </p>
</div> <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>