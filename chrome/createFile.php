<?php

//creates csv file with intent recommendations and utterances

require( "../functions.php" );

//echo "start time: {$date}<br>";

header( 'Content-Type: text/csv' );
header( "Content-Disposition: attachment; filename={$_POST[ 'recommendation' ]}.csv" );

$utterances = explode( ",", $_POST[ 'utterances' ] );
$occurences = explode( ",", $_POST[ 'occurences' ] );

$ind = 0;

foreach ( $utterances as $u ) {

    $o = $occurences[ $ind ];

    if ( !$o ) {
        $o = 1;
    }
    
    $user_CSV[ $ind ] = array( $u, $o );

    $ind++;
}

$fp = fopen("output/{$_POST[ 'recommendation' ]}.csv", 'w');
//$fp = fopen( 'php://output', 'w' ); //opens in browser

foreach ( $user_CSV as $line ) {

    fputcsv( $fp, $line, ',' );
}

fclose( $fp );

echo "file created successfully";

?>