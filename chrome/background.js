chrome.runtime.onMessage.addListener(function (request, sender, callback) {

  if (request.action == "xhttp") {
    $.ajax({
      type: request.method,
      url: request.url,
      data: request.data,
      success: function (responseText) {

        //console.log("success");

        callback(responseText);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        callback("error");
      }
    });

    return true;
  } else {
    console.log("background.js got a message")
    console.log(request);
    console.log(sender);
    callback("bar");
  }

  function callback(message) {

    console.log("callback: " + message);

    var obj = JSON.parse(message);

    console.log("response: " + obj.response);

    //chrome.runtime.sendMessage(obj.response);
    chrome.runtime.sendMessage(obj);
  }
});
