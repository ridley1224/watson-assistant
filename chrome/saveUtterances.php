<?php

//saves intent recommendations to db

require_once( '../watsonscraper.php' );
include( "../functions.php" );
include( "../en-de.php" );

if ( isset( $_POST[ 'recommendation' ] ) ) {

  $uid = de($_POST[ 'uid' ]);
  $workspaceid = $_POST[ 'workspaceid' ];

  mysql_select_db( $database_watsonscraper, $watsonscraper );

  $query_rsRecommendationInfo = sprintf( "SELECT * FROM recommendations WHERE recommendation = %s AND userid = %s AND workspaceid = %s",
    GetSQLValueString( $_POST[ 'recommendation' ], "text" ),
    GetSQLValueString( $uid, "int" ),
    GetSQLValueString( $workspaceid, "int" ) );

  $rsRecommendationInfo = mysql_query( $query_rsRecommendationInfo, $watsonscraper )or die( mysql_error() );
  $row_rsRecommendationInfo = mysql_fetch_assoc( $rsRecommendationInfo );
  $totalRows_rsRecommendationInfo = mysql_num_rows( $rsRecommendationInfo );

  if ( $totalRows_rsRecommendationInfo > 0 ) {

    //if intent saved, delete utterances

    $last_id = $row_rsRecommendationInfo[ 'recid' ];

    $deleteSQL = sprintf( "DELETE FROM recomendationutterances WHERE recid = %s",
      GetSQLValueString( $last_id, "int" ));

    mysql_select_db( $database_watsonscraper, $watsonscraper );
    $Result1 = mysql_query( $deleteSQL, $watsonscraper )or die( mysql_error() );

  } else {

    $insertSQL = sprintf( "INSERT INTO recommendations (recommendation,userid,workspaceid,datecreated) VALUES (%s,%s,%s,%s)",
      GetSQLValueString( $_POST[ 'recommendation' ], "text" ),
      GetSQLValueString( $uid, "int" ),
      GetSQLValueString( $workspaceid, "text" ),
      GetSQLValueString( $date, "date" ) );

    mysql_select_db( $database_watsonscraper, $watsonscraper );
    $Result1 = mysql_query( $insertSQL, $watsonscraper )or die( mysql_error() );
    $last_id = mysql_insert_id();
  }

  //loop through utterances and save

  $utterances = explode( ",", $_POST[ 'utterances' ] );
  $occurences = explode( ",", $_POST[ 'occurences' ] );

  $i = 0;

  foreach ( $utterances as $u ) {

    $o = $occurences[ $i ];

    if ( !$o ) {
      $o = 1;
    }

    $insertSQL2 = sprintf( "INSERT INTO recomendationutterances (recid,utterance,occurrences,datecreated) VALUES (%s,%s,%s,%s)",
      GetSQLValueString( $last_id, "int" ),
      GetSQLValueString( $u, "text" ),
      GetSQLValueString( $o, "int" ),
      GetSQLValueString( $date, "date" ) );

    mysql_select_db( $database_watsonscraper, $watsonscraper );
    $Result1 = mysql_query( $insertSQL2, $watsonscraper )or die( mysql_error() );
    $i++;
  }

  $myObj = new stdClass;
  $status = "recommendation saved";

  $myObj->response = $status;

} else {
  $myObj->response = "recommendation not saved for {$_POST[ 'recommendation' ]}";
}

echo json_encode( $myObj );

?>