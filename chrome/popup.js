var exportStarted = false;
var currentInd = 0;
var div;
var uid;
//var url = "http://localhost:8888/watson/";
var url = "http://localhost:8888/ridtech/watson/";
//var url = "http://localhost/sites/watson/";
var workspacename;

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {

  //alert("message received");

  if (message == "resetStartBtn") {
    document.querySelector('#startBtn').innerHTML = "Start Export";
  } else if (message == "updateCount") {
    var val = currentInd++;
    updatestatusDiv(val);

  } else if (message.response == "workspaces successful") {

    //populate list

    populateWorkspaceList(message.workspaces);


  } else if (message.response == "login successful") {

    document.querySelector('#userInfo').innerHTML = "Logged in as <span class=\"name\">" + message.username + "</span>";

    chrome.storage.sync.set({
      'loggedin': true
    }, function () {});

    chrome.storage.sync.set({
      'username': message.username
    }, function () {

      loginUI();
    });

    uid = message.uid;
      


    chrome.tabs.query({
      currentWindow: true,
      active: true
    }, function (tabs) {

      chrome.tabs.sendMessage(tabs[0].id, 'userLoggedIn')
    })


    chrome.storage.sync.set({
      'uid': uid
    }, function () {});


    chrome.storage.sync.get(['workspacename'], function (result) {

      if (result.workspacename) {

        workspacename = result.workspacename;
        getWorkspaces();
      }
    });

  } else if (message.response == "login failed") {

    //document.querySelector('#statusDiv').innerHTML = "<span class=\"fail\">Login unsuccessful</span>";
    document.querySelector('#failTR').style.display = "block";
  } else if (message == "saveRemote") {
    document.querySelector('#saveLocal').checked = false;
    document.querySelector('#saveLocation').innerHTML = "";

  } else if (message == "saveLocal") {

    document.querySelector('#saveLocal').checked = true;
    document.querySelector('#saveLocation').innerHTML = "Save to Downloads";
  }


});

function updatestatusDiv(countP) {

  var displayVal = parseInt(countP) + 1;

  var pd = document.querySelector('#statusDiv');
  pd.style.display = "block";
  pd.innerHTML = displayVal + " intents processed";
}

document.addEventListener('DOMContentLoaded', function () {

  chrome.storage.sync.set({
    'processedCount': 0
  }, function () {

    console.log(currentInd + ' intents processed');
  });

  //console.log("init");

  //https://developer.chrome.com/extensions/storage
  //https://developer.chrome.com/extensions/content_scripts
  //https://www.youtube.com/watch?v=Ipa58NVGs_c&t=847s
  //https://developer.chrome.com/extensions/xhr
  //https://developer.chrome.com/apps/messaging
  //https://stackoverflow.com/questions/18374252/how-do-i-pass-back-data-from-a-remote-window-to-a-chrome-extensions-background


  //load saved data

  chrome.storage.sync.get(['processedCount'], function (result) {

    if (result.processedCount) {
      currentInd = parseInt(result.processedCount);

      updatestatusDiv(currentInd);
    } else {
      currentInd = 0;
    }
  });

  //login status

  chrome.storage.sync.get(['loggedin'], function (result) {

    if (result.loggedin == true) {

      chrome.tabs.query({
        currentWindow: true,
        active: true
      }, function (tabs) {

        chrome.tabs.sendMessage(tabs[0].id, 'userLoggedIn')
      })

      loginUI();

      chrome.storage.sync.get(['workspacename'], function (result) {

        if (result.workspacename) {

          workspacename = result.workspacename;
          getWorkspaces();
        }
      });

    } else {
      logoutUI();
    }
  });

  //export prefs

  chrome.storage.sync.get(['saveLocal'], function (result) {

    //console.log('save local currently is ' + result.saveLocal);

    //init save prefs variable

    if (result.saveLocal == true) {
      document.querySelector('#saveLocal').checked = true;


      document.querySelector('#saveLocation').innerHTML = "Save to Downloads";

    } else {
      document.querySelector('#saveLocal').checked = false;

      document.querySelector('#saveLocation').innerHTML = "";
    }
  });

  //export in progress

  chrome.storage.sync.get(['exportStarted'], function (result) {

    if (result.exportStarted == true) {
      document.querySelector('#startBtn').innerHTML = "Stop Export";
      exportStarted = true;
    } else {
      document.querySelector('#startBtn').innerHTML = "Start Export";
      exportStarted = false;
    }


  });

  //populate username 

  chrome.storage.sync.get(['username'], function (result) {

    if (result.username) {

      document.querySelector('#userInfo').innerHTML = "Logged in as <span class=\"name\">" + result.username + "</span>";
    }

  });

  chrome.storage.sync.get(['uid'], function (result) {

    if (result.uid) {

      uid = result.uid;
    }

  });

  //button actions

  document.querySelector('#workspaces').addEventListener('change', selectWorkspace, false)

  function selectWorkspace() {

    var widP = document.querySelector('#workspaces').value

    chrome.storage.sync.set({
      'workspaceid': widP
    }, function () {

      console.log('current workspace id saved');
    });
  }

  document.querySelector('#startBtn').addEventListener('click', startClick, false)

  function startClick() {

    chrome.tabs.query({
      currentWindow: true,
      active: true
    }, function (tabs) {

      if (exportStarted == true) {
        exportStarted = false;

        chrome.tabs.query({
          currentWindow: true,
          active: true
        }, function (tabs) {

          document.querySelector('#startBtn').innerHTML = "Resume Export";

          chrome.tabs.sendMessage(tabs[0].id, 'stopScraper', setCount)
        })

        chrome.storage.sync.set({
          'exportStarted': false
        }, function () {

          console.log('export stopped');
        });
      } else {
        //console.log("go");

        //validates export process can start

        chrome.tabs.sendMessage(tabs[0].id, 'startScraper', startExport)
      }
    })
  }

  function startExport(res) {

    if (res != null) {

      var startStatus = `${res.startStatus}`;
      var hasRec = `${res.hasRec}`;
      var recVisible = `${res.recVisible}`;

      if (startStatus == "true") {
        exportStarted = true;

        //alert("change button to stop export");

        document.querySelector('#startBtn').innerHTML = "Stop Export";

        chrome.storage.sync.set({
          'exportStarted': true
        }, function () {

          console.log('export started');
        });
      } else if (recVisible == "false") {
        alert("Please click the \"View intent recommendations\" button in Watson Assistant.");
      } else if (hasRec == "false") {
        alert("You currently don't have intent recommendations. Please click the \"Upload CSV files\" button in Watson Assistant.");
      } else if (hasRec == "true") {
        alert("You have reached the end of the list. Scroll the intent list to load more.");
      }
      // else if(hasRec == "true")
      // {
      //   alert("Scroll to see more.");
      // }
      else {
        alert("no: " + startStatus);
      }

    }
  }

  document.querySelector('#saveLocal').addEventListener('click', onclickSaveLocal, false)

  function onclickSaveLocal() {
    chrome.tabs.query({
      currentWindow: true,
      active: true
    }, function (tabs) {

      chrome.tabs.sendMessage(tabs[0].id, 'manageSaveLocal')
    })
  }

  document.querySelector('#settingsBtn').addEventListener('click', openSettings, false)

  function openSettings() {

    window.open(url + "my-workspaces.php?uid=" + encodeURIComponent(uid) + "&af=" + encodeURIComponent("aklafd") + "&vx=" + encodeURIComponent("5ljkla=a2lksfjk") + "&sk=" + encodeURIComponent("2lj=lkajfdlkjs") + "&wid=1", "_blank");
  }

  document.querySelector('#createAccountBtn').addEventListener('click', createAccount, false)

  function createAccount() {

    window.open(url + "create-account.php", "_blank");
  }


  document.querySelector('#login').addEventListener('click', login1, false)

  function login1() {

    //chrome.runtime.sendMessage("login2");

    chrome.tabs.query({
      currentWindow: true,
      active: true
    }, function (tabs) {

      var username = document.querySelector('#email').value;
      var password = document.querySelector('#password').value;

      chrome.tabs.sendMessage(tabs[0].id, {
        username: username,
        password: password,
        login: "true"
      })
    })
  }

  function loginRes(res) {

    var status = `${res.loginStatus1}`;

    if (status == "true") {
      loginUI();

      chrome.storage.sync.set({
        'loggedin': true
      }, function () {

        console.log('loginStatus1 is set to ' + true);
      });
    } else {
      logoutUI();
    }
  }

  document.querySelector('#logoutBtn').addEventListener('click', logout1, false)

  function logout1() {

    chrome.tabs.query({
      currentWindow: true,
      active: true
    }, function (tabs) {

      chrome.tabs.sendMessage(tabs[0].id, 'logout')

      logoutUI();

      chrome.storage.sync.set({
        'loggedin': false
      }, function () {

        console.log('loginStatus1 is set to ' + false);
      });
    })
  }

  function setCount(res) {

    currentInd = `${res.count}`;

    if (currentInd > 0) {
      updatestatusDiv(currentInd);
    }
  }

}, false)

function populateWorkspaceList(obj) {

  var select = document.querySelector('#workspaces');
  var i;

  for (i = 0; i < obj.length; i++) {

    var rd = obj[i];

    var name = rd.workspacename;
    var wid = rd.workspaceid;

    var option = document.createElement("option");
    option.text = name;
    option.value = wid;

    if (workspacename == name) {
      option.selected = true;


      chrome.storage.sync.set({
        'workspaceid': wid
      }, function () {

        console.log('current workspace id saved');
      });
    }

    select.add(option);
  }
}

function getWorkspaces() {

  chrome.tabs.query({
    currentWindow: true,
    active: true
  }, function (tabs) {

    chrome.tabs.sendMessage(tabs[0].id, {
      uid: uid,
      workspaces: "true"
    })
  })
}

function loginUI() {

  document.querySelector('#saveOptionsDiv').style.display = "block";
  document.querySelector('#userInfo').style.display = "block";
  document.querySelector('#saveOptionsDiv').style.display = "block";
  document.querySelector('#settingsDiv').style.display = "block";
  document.querySelector('#workspaceDiv').style.display = "block";


  document.querySelector('#statusDiv').innerHTML = "";

  document.querySelector('#form').style.display = "none";
  document.querySelector('#loginDiv').style.display = "none";
  document.querySelector('#newAccountDiv').style.display = "none";
  document.querySelector('#failTR').style.display = "none";
}

function logoutUI() {

  document.querySelector('#saveOptionsDiv').style.display = "none";
  document.querySelector('#userInfo').style.display = "none";
  document.querySelector('#saveOptionsDiv').style.display = "none";
  document.querySelector('#settingsDiv').style.display = "none";
  document.querySelector('#workspaceDiv').style.display = "none";

  document.querySelector('#form').style.display = "block";
  document.querySelector('#loginDiv').style.display = "block";
  document.querySelector('#newAccountDiv').style.display = "block";
}
