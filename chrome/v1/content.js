var currentInd = 0;
var timer;
var running;
var currentTitle;
var utterances;
var occurences;
var offset = 0;
var respV;

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {

    //currentInd = 0;

    if (request == "hi") {
        
        const re = new RegExp('bears', 'gi')
        const matches = document.documentElement.innerHTML.match(re)

        var total = 0;

        if (matches != null) {

            total = matches.length;
        }

        sendResponse({
            count: total
        })
    } else if (request == "getRecommendationTitles") {
        var total = 0;

        var recommendationDivs = document.getElementsByClassName("bx--link ClusterList__card-containter--link");
        //var titleDiv = recommendationDivs[0].querySelector('.ClusterCard__title');

        if (recommendationDivs != null) {
            //console.log("has divs");
            total = recommendationDivs.length;

            var i;
            for (i = 0; i < total; i++) {

                var rd = recommendationDivs[i];

                var titleDiv = rd.querySelector('.ClusterCard__title');
                var link = rd.href;

                if (titleDiv != null) {
                    console.log("title: " + titleDiv.innerHTML);

                    //console.log("has title: " + titleDiv.innerHTML);
                }
            }
        } else {

            //console.log("no divs");
        }

        console.log("recommendationDivs count: " + total);

        sendResponse({
            count: total
        })
    } else if (request == "clickDiv1") {
        clickDiv();
    } else if (request == "getUtterances") {
        // get recommendation utterances

        getUtterances();
    } else if (request == "goBack") {
        goBack();
    } else if (request == "startScraper") {
        startScraper();
    } else if (request == "ranOne") {
        console.log("ran one");
    } else if (request == "stopScraper") {
        //stopScraper();

        running = false;

        console.log("currentInd: " + currentInd);

        sendResponse({
            
            count: currentInd
        })
    }
    else if (request == "getCurrentCount") {

        console.log("currentInd: " + currentInd);

        sendResponse({
            
            count: currentInd
        })
    }
})

function getRecommendationTitles() {}

function startScraper() {

    console.log("start");

    running = true;

    var btns = document.getElementsByClassName("bx--btn bx--btn--secondary");
    var recommendationBtn = btns[0];

    if (recommendationBtn != null) {

        recommendationBtn.click();

        setTimeout(function () {
            clickDiv();
        }, 1000);
    }
    else
    {
        alert("You currently don't have intent recommendations available.");
    }
}

function clickDiv() {

    if (running) {
        
        var recommendationDivs = document.getElementsByClassName("bx--link ClusterList__card-containter--link");

        setTimeout(function () {
            getUtterances();
        }, 3000);

        var div1 = recommendationDivs[currentInd];

        if (div1 != null) {
                        
            if(currentInd % 3 == 0 && currentInd > 0)
            {
                //after every 3rd row is processed, scroll view
                
                div1.scrollIntoView();
            }

            var titleDiv = div1.querySelector('.ClusterCard__title');

            if (titleDiv != null) {

                //console.log("title: " + titleDiv.innerHTML);
                currentTitle = titleDiv.innerHTML;

                //return;

                //console.log("has title: " + titleDiv.innerHTML);
            }

            div1.click();
        }
        else
        {
            alert("You currently don't have intent recommendations available.");
        }
    }
}

function getUtterances() {

    var total = 0;
    utterances = [];
    occurences = [];

    var utteranceTable = document.getElementsByClassName("bx--data-table-v2 bx--data-table-v2--zebra");

    if (utteranceTable[0].rows != null) {
        total = utteranceTable[0].rows.length;
        //console.log("row length: " + total);

        var i;
        for (i = 0; i < total; i++) {

            var utterance = utteranceTable[0].rows[i].cells[1].innerHTML;
            var occurence = utteranceTable[0].rows[i].cells[2].innerHTML;

            //console.log(utterance + " " + occurences);

            if (i > 0) {

                utterances.push(utterance);
                occurences.push(occurence);
            }
        }

        saveUtterances();
    }
    
    goBack();
}

function goBack() {

    //console.log(goBack);

    currentInd++;

    setTimeout(function () {
        clickDiv();
    }, 1000);

    var backBtn = document.getElementsByClassName("ClusterHeader__breadcrumb bx--btn bx--btn--primary");
    backBtn[1].click();
}

function saveUtterances() {

    var rows = [];

    var header = ["utterance","occurances"];
    rows.push(header);

    for (var i = 0; i < utterances.length; i++) {

        var row = [utterances[i],occurences[i]];
        rows.push(row);
    }
    
    let csvContent = "data:text/csv;charset=utf-8," 
        + rows.map(e => e.join(",")).join("\n");

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", currentTitle + ".csv");
    document.body.appendChild(link); // Required for FF

    link.click(); // This will download the data file named.
}

function saveUtterances1() {

    var u = utterances.join();
    var o = occurences.join();

    chrome.runtime.sendMessage({
        method: 'POST',
        action: 'xhttp',
        url: 'http://localhost:8888/watson/chrome/saveUtterances.php',
        dataType: "JSON",
        data: {
            recommendation: currentTitle,
            utterances: u,
            occurences: o
        }
    });
}
