var currentInd = 0;
var timer;
var running;
var currentTitle;
var utterances;
var occurences;
var offset = 0;
var respV;
var loggedIn;
var saveLocal;
var uid;
var workspaceid;
var username;
var password;
//var url = "http://localhost:8888/watson/chrome/";
var url = "http://localhost:8888/ridtech/watson/chrome/";
//var url = "http://localhost/sites/watson/chrome/";

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {

  if (request == "countMatches") {

    const re = new RegExp('bears', 'gi')
    const matches = document.documentElement.innerHTML.match(re)

    var total = 0;

    if (matches != null) {

      total = matches.length;
    }

    sendResponse({
      count: total
    })
  } else if (request == "getRecommendationTitles") {
    var total = 0;

    var recommendationDivs = document.getElementsByClassName("bx--link ClusterList__card-containter--link");
    //var titleDiv = recommendationDivs[0].querySelector('.ClusterCard__title');

    if (recommendationDivs != null) {
      //console.log("has divs");
      total = recommendationDivs.length;

      var i;
      for (i = 0; i < total; i++) {

        var rd = recommendationDivs[i];

        var titleDiv = rd.querySelector('.ClusterCard__title');
        var link = rd.href;

        if (titleDiv != null) {
          console.log("title: " + titleDiv.innerHTML);

          //console.log("has title: " + titleDiv.innerHTML);
        }
      }
    } else {

      //console.log("no divs");
    }

    console.log("recommendationDivs count: " + total);

    sendResponse({
      count: total
    })
  } else if (request == "clickDiv1") {
    clickDiv();
  } else if (request == "getUtterances") {
    // get recommendation utterances

    getUtterances();
  } else if (request == "goBack") {
    goBack();
  } else if (request == "startScraper") {

    chrome.storage.sync.get(['processedCount'], function (result) {

      if (result.processedCount) {
        currentInd = parseInt(result.processedCount);
      } else {
        currentInd = 0;
      }

      console.log("currentInd query: " + currentInd);

    });

    var recommendationDivs = document.getElementsByClassName("bx--link ClusterList__card-containter--link");
    var div1 = recommendationDivs[currentInd];
    //var div1 = recommendationDivs[0];

    var btns = document.getElementsByClassName("bx--btn bx--btn--secondary");


    //check if recommendations have been loaded

    var recommendationsVisible = false;

    //ClusterList__summary //indicates intent recommendations window is open

    var recWindow = document.getElementsByClassName("ClusterList__summary");

    console.log("recWindow: " + recWindow);

    //if (recommendationDivs[0] != null) {
    if (recWindow.length > 0) {

      recommendationsVisible = true;
    }

    //check if there are recommendations

    var recommendationBtn = btns[0];

    var hasRecommendations = false;

    var recommendationCount = recommendationDivs.length;

    //if (recommendationBtn != null) {
    if (recommendationCount > 0) {

      hasRecommendations = true;
    }


    console.log("currentInd: " + currentInd);
    console.log("recVis: " + recommendationsVisible);
    console.log("hasRec: " + hasRecommendations);
    console.log("recommendationDivs: " + recommendationCount);
    //console.log("div1: "+div1);

    var status = false;

    if (div1 != null) {

      status = true;
      startScraper();
    }

    //console.log("status: "+status);

    sendResponse({

      startStatus: status,
      hasRec: hasRecommendations,
      recVisible: recommendationsVisible,
      recCount: recommendationCount
    })

  } else if (request == "stopScraper") {
    //stopScraper();

    running = false;

    console.log("currentInd: " + currentInd);

    sendResponse({

      count: currentInd
    })
  } else if (request == "manageSaveLocal") {

    var saveLocal;

    chrome.storage.sync.get(['saveLocal'], function (result) {

      console.log('current stored saveLocal ' + result.saveLocal);

      if (result.saveLocal == true) {
        saveLocal = false;
        chrome.runtime.sendMessage("saveRemote");
      } else {
        saveLocal = true;
        chrome.runtime.sendMessage("saveLocal");
      }

      chrome.storage.sync.set({
        'saveLocal': saveLocal
      }, function () {

        console.log('saveLocal is set to ' + saveLocal);
      });

      chrome.storage.sync.get(['saveLocal'], function (result) {

        console.log('new saveLocal is ' + result.saveLocal);
      });
    });

    // sendResponse({

    //     count: currentInd
    // })
  } else if (request == "logout") {

    chrome.storage.sync.set({
      'loggedin': false
    }, function () {

      console.log('loggedin is set to ' + false);
    });
  } else if (request.login == "true") {

    console.log("login: username: " + request.username + " password: " + request.password);

    username = request.username;
    password = request.password;

    loginUser();

  } else if (request.workspaces == "true") {


    var skillHeader = document.getElementsByClassName("SkillSubheader_skill-details__header");

    //console.log("skillHeader: "+skillHeader);

    var header = skillHeader[0];

    //console.log("header: " + header);

    var h3Elements = header.getElementsByTagName("h3");

    //console.log("h3Elements: " + h3Elements);

    var h3 = h3Elements[0];

    var workspacename = h3.innerHTML;

    //console.log("workspacename: " + h3.innerHTML);

    chrome.storage.sync.set({
      'workspacename': workspacename
    }, function () {

      console.log("current workspace " + workspacename + " saved");
    });
      
            console.log("workspaces uid: "+request.uid);


    //console.log("getWorkspaces1");

    getWorkspaces1();

  } else if (request == "getCurrentCount") {

    console.log("currentInd: " + currentInd);

    sendResponse({

      count: currentInd
    })
  } else if (request == "userLoggedIn") {

    console.log("user logged in");
    loggedIn = true;
  }
})

function getRecommendationTitles() {}

function startScraper() {

  console.log("start scaper");
    
    running = true;

    //recommendationBtn.click();

    setTimeout(function () {
      clickDiv();
    }, 1000);

//  var btns = document.getElementsByClassName("bx--btn bx--btn--secondary");
//  var recommendationBtn = btns[0];
//
//  if (recommendationBtn != null) {
//
//    //validates export process can start
//
//    running = true;
//
//    recommendationBtn.click();
//
//    setTimeout(function () {
//      clickDiv();
//    }, 1000);
//  } else {
//    alert("You currently don't have intent recommendations available.");
//  }
}

function clickDiv() {

  if (running) {

    var recommendationDivs = document.getElementsByClassName("bx--link ClusterList__card-containter--link");

    setTimeout(function () {
      getUtterances();
    }, 3000);

    var div1 = recommendationDivs[currentInd];

    if (div1 != null) {

      if (currentInd % 3 == 0 && currentInd > 0) {
        //after every 3rd row is processed, scroll view

        div1.scrollIntoView();
      }

      var titleDiv = div1.querySelector('.ClusterCard__title');

      if (titleDiv != null) {

        //console.log("title: " + titleDiv.innerHTML);
        currentTitle = titleDiv.innerHTML;
      }

      div1.click();
    } else {
      if (currentInd > 0) {
        chrome.runtime.sendMessage("resetStartBtn");

        alert("You have reached the end of the list. Scroll the intent list to load more.");
      } else {
        alert("You currently don't have intent recommendations available.");
      }
    }
  }
}

function getUtterances() {

  var total = 0;
  utterances = [];
  occurences = [];

  var utteranceTable = document.getElementsByClassName("bx--data-table-v2 bx--data-table-v2--zebra");

  if (utteranceTable[0].rows != null) {
    total = utteranceTable[0].rows.length;
    //console.log("row length: " + total);

    var i;
    for (i = 0; i < total; i++) {

      var utterance = utteranceTable[0].rows[i].cells[1].innerHTML;
      var occurence = utteranceTable[0].rows[i].cells[2].innerHTML;

      //console.log(utterance + " " + occurences);

      if (i > 0) {

        utterances.push(utterance);
        occurences.push(occurence);
      }
    }

    chrome.storage.sync.get(['saveLocal'], function (result) {

      if (result.saveLocal == true) {
        console.log("save local");
        saveUtterancesLocal();
      }

      if (loggedIn == true) {
        console.log("save remote");

        chrome.storage.sync.get(['workspaceid'], function (result) {

          if (result.workspaceid) {

            workspaceid = result.workspaceid;
              
              chrome.storage.sync.get(['uid'], function (result) {
                  
                  uid = result.uid;
                  
                        console.log("saving uid: "+uid);

                  
                saveUtterancesUserAccount();
                  
              });
          }
        });
      }
    });

    //saveUtterancesLocal();

    chrome.storage.sync.set({
      'processedCount': currentInd
    }, function () {

      console.log(currentInd + ' intents processed');
    });

    chrome.runtime.sendMessage("updateCount");
  }

  goBack();
}

function goBack() {

  //console.log(goBack);

  currentInd++;

  setTimeout(function () {
    clickDiv();
  }, 1000);

  var backBtn = document.getElementsByClassName("ClusterHeader__breadcrumb bx--btn bx--btn--primary");
  backBtn[1].click();
}

function saveUtterancesLocal() {

  var rows = [];

  var header = ["utterance", "occurances"];
  rows.push(header);

  for (var i = 0; i < utterances.length; i++) {

    var row = [utterances[i], occurences[i]];
    rows.push(row);
  }

  let csvContent = "data:text/csv;charset=utf-8,"
    + rows.map(e => e.join(",")).join("\n");

  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", currentTitle + ".csv");
  document.body.appendChild(link); // Required for FF

  link.click(); // This will download the data file named.
}

function saveUtterancesUserAccount() {

  console.log("saveUtterancesUserAccount");

  var u = utterances.join();
  var o = occurences.join();

  chrome.runtime.sendMessage({
    method: 'POST',
    action: 'xhttp',
    url: url + 'saveUtterances.php',
    dataType: "JSON",
    data: {
      recommendation: currentTitle,
      uid: uid,
      workspaceid: workspaceid,
      utterances: u,
      occurences: o
    }
  });
}

function loginUser() {

  chrome.runtime.sendMessage({
    method: 'POST',
    action: 'xhttp',
    url: url + 'login.php',
    dataType: "JSON",
    data: {
      username: username,
      password: password
    }
  });
}

function getWorkspaces1() {

  chrome.runtime.sendMessage({
    method: 'POST',
    action: 'xhttp',
    url: url + 'workspaces.php',
    dataType: "JSON",
    data: {
      uid: uid
    }
  });
}
