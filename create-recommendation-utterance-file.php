<?php

require_once( 'watsonscraper.php' );
include( "functions.php" );


$colname_rsWorkspaceDetails = "-1";
if ( isset( $_GET[ 'wid' ] ) ) {
  $colname_rsWorkspaceDetails = $_GET[ 'wid' ];
}

mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsWorkspaceDetails = sprintf( "SELECT * FROM workspaces WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsWorkspaceDetails = mysql_query( $query_rsWorkspaceDetails, $watsonscraper )or die( mysql_error() );
$row_rsWorkspaceDetails = mysql_fetch_assoc( $rsWorkspaceDetails );
$totalRows_rsWorkspaceDetails = mysql_num_rows( $rsWorkspaceDetails );

header( 'Content-Type: text/csv' );
header( "Content-Disposition: attachment; filename={$row_rsWorkspaceDetails[ 'workspacename' ]}-intent-recommendations.csv" );

header('Pragma: no-cache');
header('Expires: 0');



mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsIntents = sprintf( "SELECT * FROM recommendations WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );

$rsIntents = mysql_query( $query_rsIntents, $watsonscraper )or die( mysql_error() );
$row_rsIntents = mysql_fetch_assoc( $rsIntents );

$recInd = 0;
$totalRows = 0;

do {

    $query_rsUttterances = "SELECT * FROM recomendationutterances WHERE recid = {$row_rsIntents[ 'recid' ]} AND utterance IS NOT NULL";
    $rsUttterances = mysql_query( $query_rsUttterances, $watsonscraper )or die( mysql_error() );
    $row_rsUttterances = mysql_fetch_assoc( $rsUttterances );

    $uind = 0;

    //echo "<strong>{$row_rsIntents[ 'recommendation' ]}</strong><br>";
    
     $user_CSV[ 0 ] = array( $row_rsWorkspaceDetails[ 'workspacename' ], "" );

    $totalRows++;
    
    do {

        if($uind == 0)
        {
            $user_CSV[ $totalRows ] = array( $row_rsIntents[ 'recommendation' ],$row_rsUttterances[ 'utterance' ] );
        }
        else
        {
            $user_CSV[ $totalRows ] = array( "",$row_rsUttterances[ 'utterance' ] );
        }

        //echo "{$row_rsUttterances[ 'utterance' ]}<br>";

        $uind++;
        $totalRows++;

    } while ($row_rsUttterances = mysql_fetch_assoc($rsUttterances));

    $recInd++;

    // if($recInd > 1)
    // {
    //     break;
    // }

} while ($row_rsIntents = mysql_fetch_assoc($rsIntents));

// echo "<pre>";
// var_dump($user_CSV);
// echo "</pre>";

//$fp = fopen("recommendation_utterance_output.csv", 'w');

$fp = fopen( 'php://output', 'w' );

foreach ( $user_CSV as $line ) {

    fputcsv( $fp, $line, ',' );
}

fclose( $fp );

?>