<?php
require_once( 'watsonscraper.php' );
include( "functions.php" );

$colname_rsWorkspaceDetails = "-1";
if ( isset( $_GET[ 'wid' ] ) ) {
  $colname_rsWorkspaceDetails = $_GET[ 'wid' ];
}
else if ( isset( $_POST[ 'wid' ] ) ) {
  $colname_rsWorkspaceDetails = $_POST[ 'wid' ];
}
mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsWorkspaceDetails = sprintf( "SELECT * FROM workspaces WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsWorkspaceDetails = mysql_query( $query_rsWorkspaceDetails, $watsonscraper )or die( mysql_error() );
$row_rsWorkspaceDetails = mysql_fetch_assoc( $rsWorkspaceDetails );
$totalRows_rsWorkspaceDetails = mysql_num_rows( $rsWorkspaceDetails );

?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Watson Intents - <?php echo $row_rsWorkspaceDetails['workspacename']; ?></title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Watson Intents</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p id="workspaceLbl"><?php echo $row_rsWorkspaceDetails['workspacename']; ?></p>
    <div id="renderContent"></div>
    <form method="post" action="view-intents.php">
      <table  cellpadding="5" cellspacing="5">
        <?php if(isset($_POST['search'])){ $search = $_POST['search'];} else if(isset($_GET['search'])){ $search = $_GET['search'];} ?>
        <tr>
          <td><input type="text" name="search" placeholder="Search" class="inputBox" value="<?php echo $search;?>"></td>
        </tr>
        <tr>
          <td><input type="submit" value="Submit" name="submit" class="submitBtn"></td>
        </tr>
      </table>
          <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
<input type="hidden" name="wid" id="wid" value="<?php echo $colname_rsWorkspaceDetails; ?>"></form>
    <p id="docContentDiv">
      <?php include("includes/intentsHTML.php");?>
    </p>
    <div class="backDiv"><a href="workspace-details.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Back</a></div>
  </div>

</div>
<?php include("includes/side-nav.php");?>
</body>
</html>
<?php
mysql_free_result( $rsFiles );
?>