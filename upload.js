var hasFile;
var fileName;
var selectedColumns = [];
var selectedColumnNames = [];
var selectedColumnFormats = [];
var inputs = [];
var combos = [];

$(document).ready(function () {

  //console.log("ready3");

  //disable button

  //    $(".cb1").change(function() {
  //        
  //        if(this.checked) {
  //            
  //            //var returnVal = confirm("Are you sure?");
  //            //$(this).prop("checked", returnVal);
  //            
  //            console.log("add");
  //        }
  //        else
  //        {
  //            console.log("remove");
  //        }
  //        
  //        //$('#textbox1').val(this.checked);        
  //    });


  $("#saveBtn").css("background-color", "gray");

  $('#saveBtn').on('click', function () {

    //alert("get file");

    if (!hasFile) {

      console.log("select a file");

      //$("#languageTxt").html("Please select a file.");
      return;
    }

    saveData();

  });

  //$("#saveBtn").attr("disabled", 'disabled');

  $("#upload").on('change', function () {

    var file = $('#upload').prop('files')[0];
    var ext = $('#upload').val().split('.').pop().toLowerCase();

    if ($.inArray(ext, ['csv']) == -1) {

      //$("#fileNameTxt").html("Invalid file type");
      $("#saveBtn").css("background-color", "gray");
      //$('#languageTxt').html("Invalid file type. Only .txt, .doc, .docx and .pdf files are accepted.");
      //rgb(155, 197, 61);
      hasFile = false;

    } else {

      //$('#saveBtn').prop('disabled', false);
      hasFile = true;

      if (hasFile) {
        $("#saveBtn").css("background-color", "rgb(155,197,61)");
      }

      readURL(this);

    }
  });
});

function readURL(input) {

  //console.log("read");

  if (input.files && input.files[0]) {

    //console.log("read2");

    var reader = new FileReader();

    reader.onload = function (e) {

      fileName = input.files[0].name;

      //console.log("file: " + input.files[0].name);

      //            $("#fileNameTxt").html(fileName);
      //            $('#languageTxt').html("");

      //$('.profile-pic').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function uploadData() {

  var ind1 = 0;
  selectedColumnFormats = [];


  $('#form2 select').each(function () {
    //total+= +($(this).val());

    if (selectedColumns.includes(ind1)) {

      selectedColumnFormats.push($(this).val());
    }

    ind1++;

  });


  var ind = 0;

  $("#form2 input").each(function () {

    if ($(this).is('[type=text]')) {

      if (selectedColumns.includes(ind)) {

        //$(this).css("background-color", "green");
        //console.log("val: " + $(this).val());

        selectedColumnNames.push($(this).val());


      } else {
        //$(this).css("background-color", "red");
      }

      ind++;
    }
  });

  console.log("selectedColumnNames: " + selectedColumnNames);
  console.log("selectedColumnFormats: " + selectedColumnFormats);


  //    var x;
  //    var vals = [];
  //
  //      for (x = 0; x < selectedColumns.length; x++) {
  //          
  //          var ind = selectedColumns[0];
  //          var inputVal =  $("#submitDiv").val();
  //          
  //          
  //          vals.push();
  //          
  //
  //      }
}

function saveData() {

  console.log("save");

  var file = $('#upload').prop('files')[0];

  if (file == null) {

    console.log("select a file");

    //$('#languageTxt').html("Please select a file");
    return;
  }

  var fileType = file["type"];
  var validImageTypes = ["text/csv"];
  var fileExtensions = ['csv'];
  var ext = $('#upload').val().split('.').pop().toLowerCase();
  //var pre = $('#fileField').val().split('.')[0].toLowerCase();

  //console.log("ext: " + ext);
  //console.log("client fileType: " + fileType);

  var form_data = new FormData();


  if (0 == 1) {
    //if ($.inArray(fileType, validImageTypes) < 0 && ($.inArray(ext, fileExtensions) < 0)) {

    // invalid file type code goes here.

    //$('#languageTxt').html("Invalid file type. Only .txt, .doc, .docx and .pdf files are accepted.");

  } else {

    form_data.append('file', file);

    //$('#upload').val('');

    $.ajax({
      url: 'includes/get-columns.php', // point to server-side PHP script 
      dataType: 'text', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      error: function (xhr, status, error) {

        console.log("error: " + error);
      },
      success: function (php_script_response) {

        //console.log(php_script_response);

        var obj = jQuery.parseJSON(php_script_response);
        var cols = obj.cols;
        var colNames = obj.colNames;

        if (cols != null) {

          //console.log("cols: " + cols);

          var x;

          var html = "<tr><td>Columns</td><td>Type</td><td class=\"cb\">Select</td></tr>";

          for (x = 0; x < cols; x++) {

            var fieldName = "textfield" + x;
            var cbName = "cb" + x;

            //console.log("col\n");

            var colNameVal = colNames[x];

            var input = "<input type=\"text\" name=\"" + fieldName + "\" placeholder=\"Enter column name\" id=\"" + fieldName + "\" value=\"" + colNames[x] + "\">";

            inputs.push(input);

            var combo = "<select><option value=\"text\">Text</option><option value=\"float\">Number</option><option value=\"date\">Date</option></select>";
            combos.push(x);

            var checkedStatus = "";

            if (colNameVal != "") {
              checkedStatus = "checked";
              selectedColumns.push(x);
            }

            var cb = "<input type=\"checkbox\" class=\"cb1\" name=\"" + cbName + "\" value=\"" + x + "\" " + checkedStatus + ">";

            html += "<tr><td>" + input + "</td><td>" + combo + "</td><td class=\"cb\">" + cb + "</td></tr>";
          }

          html += "<tr><td><div id=\"nppi\"><span>Strip NPPI</span> <input type=\"checkbox\" value=\"stripNPPI\" id=\"stripNPPI\" name=\"submit\"><img src=\"img/question.png\" width=\"15\" height=\"15\"></div></td></tr><tr><td><input type=\"button\" name=\"uploadBtn\" id=\"uploadBtn\" value=\"Upload\"></td></tr>";

          //console.log("html: " + html);

          $('#colTable tbody').append(html);

          $(".cb1").change(function () {

            if (this.checked) {

              //var returnVal = confirm("Are you sure?");
              //$(this).prop("checked", returnVal);

              //console.log("add: " + this.value);

              selectedColumns.push(parseInt(this.value));

            } else {

              var index1 = selectedColumns.indexOf(parseInt(this.value));

              if (index1 > -1) {

                //console.log("remove: " + this.value);

                selectedColumns.splice(index1, 1);
              }
            }

            //$('#textbox1').val(this.checked);        
          });

          $("#uploadBtn").css("background-color", "gray");

          $('#uploadBtn').on('click', function () {

            //alert("get file");

            uploadData();

          });

          $("#submitDiv").css("display", "block");
          $("#saveBtn").css("background-color", "gray");
        }
      }
    });
  }
}
