<?php
require_once( 'watsonscraper.php' );
include( "functions.php" );
?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Utterances - <?php echo $_GET['intent']?></title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl"><?php echo $_GET['intent']?></p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p>&nbsp;</p>
    <div id="renderContent"> </div>
    <p id="docContentDiv">
      <?php include("includes/getUtterancesHTML.php");?>
    </p>
  </div>
  <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>
<?php
mysql_free_result( $rsFiles );
?>