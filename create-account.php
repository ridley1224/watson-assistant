<?php

require_once( 'watsonscraper.php' );
include( "functions.php" );

$loginFormAction = $_SERVER[ 'PHP_SELF' ];

if ( isset( $_POST[ 'username' ] ) ) {

  $loginUsername = $_POST[ 'username' ];
  $password = $_POST[ 'password' ];
  $MM_redirectLoginSuccess = "my-workspaces.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db( $database_watsonscraper, $watsonscraper );

  $LoginRS__query = sprintf( "SELECT email FROM users WHERE email=%s",
    GetSQLValueString( $loginUsername, "text" ), GetSQLValueString( $password, "text" ) );

  $LoginRS = mysql_query( $LoginRS__query, $watsonscraper )or die( mysql_error() );
  $row_rsUserInfo = mysql_fetch_assoc( $LoginRS );
  $loginFoundUser = mysql_num_rows( $LoginRS );

  if ( strlen( $_POST[ 'username' ] ) < 1 ) {
      
      $status = "Please enter your email address.";
  }
else if ( strlen( $_POST[ 'password' ] ) < 1 ) {
      
      $status = "Please enter your password.";
  }
    
  else if ( $loginFoundUser ) {

    $status = "User with account {$loginUsername} already exists. Please choose another email.";

  } else {

    $loginStrGroup = "";

    $insertSQL = sprintf( "INSERT INTO users (email, password,datecreated) VALUES (%s,%s,%s)",
      GetSQLValueString( $loginUsername, "text" ),
      GetSQLValueString( $password, "text" ),
      GetSQLValueString( $_SESSION[ 'uid' ], "int" ),
      GetSQLValueString( $date, "date" ) );

    mysql_select_db( $database_watsonscraper, $watsonscraper );
    $Result1 = mysql_query( $insertSQL, $watsonscraper )or die( mysql_error() );
    $last_id = mysql_insert_id();


    if ( PHP_VERSION >= 5.1 ) {
      session_regenerate_id( true );
    } else {
      session_regenerate_id();
    }

    //declare two session variables and assign them
    $_SESSION[ 'MM_Username' ] = $loginUsername;
    $_SESSION[ 'uid' ] = $last_id;

    header( "Location: " . $MM_redirectLoginSuccess );

  }
}
?>
<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<script src="jquery/jquery-1.11.1.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>Create Account</title>
</head>

<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <div id="headerBG" class="clearfix"> <a href="index.php"><img id="logo" src="img/wa48.png" class="image"/></a> </div>
  <div id="titleDiv2" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Create Account</p>
    </div>
  </div>
  <div id="contentBG2" class="clearfix">
    <form action="<?php echo $loginFormAction; ?>" id="form1" name="form1" method="POST">
      <p>&nbsp;</p>
      <table width="100%" cellpadding="5" cellspacing="5">
        <tbody>
          <tr>
            <td width="12%">Email</td>
            <td width="88%"><input name="username" type="text" id="username" value="<?php if(isset($_POST['username'])) {echo $_POST['username'];} ?>" class="inputBox"></td>
          </tr>
          <tr>
            <td>Password</td>
            <td><input name="password" type="password" id="password" value="" class="inputBox"></td>
          </tr>
          <?php if(isset($status)){ ?>
          <tr>
            <td>&nbsp;</td>
            <td><span class="status"><?php echo $status;?></span></td>
          </tr>
          <?php } ?>
          <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" id="submit" value="Submit" class="submitBtn2"></td>
          </tr>
        </tbody>
      </table>
      <input type="hidden" name="MM_update" value="form1">
</form>
    <p id="docContentDiv">&nbsp;</p>
  </div>
</div>
</body>
</html>