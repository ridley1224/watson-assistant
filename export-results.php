<?php
require_once( 'watsonscraper.php' );
include( "functions.php" );

$colname_rsWorkspaceDetails = "-1";
if ( isset( $_GET[ 'wid' ] ) ) {
  $colname_rsWorkspaceDetails = $_GET[ 'wid' ];
}
mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsWorkspaceDetails = sprintf( "SELECT * FROM workspaces WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsWorkspaceDetails = mysql_query( $query_rsWorkspaceDetails, $watsonscraper )or die( mysql_error() );
$row_rsWorkspaceDetails = mysql_fetch_assoc( $rsWorkspaceDetails );
$totalRows_rsWorkspaceDetails = mysql_num_rows( $rsWorkspaceDetails );
?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Export Recommendation Results - <?php echo $row_rsWorkspaceDetails['workspacename']; ?></title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Export Recommendation Results</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p id="workspaceLbl"><?php echo $row_rsWorkspaceDetails['workspacename']; ?></p>
    <div id="renderContent"> Download your Intent Recommendation results to a single csv file. </div>
    <p id="createFileDiv"><a href="create-recommendation-utterance-file.php?wid=<?php echo $colname_rsWorkspaceDetails; ?>">Export</a></p>
    <p id="docContentDiv"> </p>
  </div>
  <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>
<?php
mysql_free_result( $rsFiles );
?>