<?php

$curl = curl_init();

if(isset($_GET['next']))
{
    $url = $_GET['next'];
    $previous = $url;
}
else
{
    $url = "/v1/workspaces/5bc066ac-4179-440c-b201-dae320b9f400/logs?version=2017-05-26&filter=response_timestamp%3E2019-09-26";
}

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://gateway.watsonplatform.net/conversation/api{$url}",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Accept: */*",
    "Accept-Encoding: gzip, deflate",
    "Connection: keep-alive",
    "Host: gateway.watsonplatform.net",
    "Postman-Token: b24f0f5c-5117-4b22-97c8-49be8a95233a",
    "User-Agent: PostmanRuntime/7.16.3",
    "authorization: Basic OWYzOTQ2YWMtMzNkYy00NjdkLWEwMDktYWEyNTkzMmNkMmVhOmY0ZkdwbGN4cExObA==",
    "cache-control: no-cache,no-cache",
    "postman-token: 6b9b8e97-2280-54b1-3a91-bedfdc1bec8d"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;

  $decodedData = json_decode($response);
  $logs = $decodedData->logs;
  $pagination = $decodedData->pagination->next_url;
 
//   echo "<pre>";
//   var_dump($logs);
//   echo "</pre>";

  //echo "total: " . count($logs) . "<br>";

  if($previous)
    {
    echo "<p><a href='watson-logs-parse.php?previous={$previous}'>Previous</a></p>";
    }


  if($pagination)
  {
    $link = urlencode($pagination);
      //echo $pagination;
      
        echo "<p><a href='watson-logs-parse.php?next={$link}'>Next</a></p>";
  }

  foreach($logs as $log)
  {
      echo "<strong>user input:</strong> " . $log->request->input->text . "<br>";

      $responses = $log->response->output->generic[0]->text;

    //   foreach($responses as $response)
    //     {

    //     }

    echo "<strong>watson response:</strong> " . strip_tags($responses) . "<br>";
    echo "<strong>intent:</strong> " . $log->response->intents[0]->intent . "<br>";
    echo "<strong>time:</strong> " . $log->response_timestamp . "<br><br>";
  }

  if($previous)
      {
        echo "<p><a href='watson-logs-parse.php?previous={$previous}'>Previous</a></p>";
      }

  if($pagination)
  {
    $link = urlencode($pagination);
      //echo $pagination;
      
        echo "<p><a href='watson-logs-parse.php?next={$link}'>Next</a></p>";
  }
}

?>