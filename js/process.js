var xmlhttp;
var state;
var debug = true;
var init = true;
var hasChats = false;


function sendMessage() {

	console.log("send message");

	xmlhttp=null;

	var Url="utteranceAJAX.php";

	console.log("url: "+Url);

	//alert(Arg);

	if (window.XMLHttpRequest) {

		//console.log("modern");

		xmlhttp=new XMLHttpRequest() ;                   // For all modern browsers

	} else if (window.ActiveXObject) {

		//console.log("old");

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")   // For (older) IE
	}

	if (xmlhttp!=null)
	{
		xmlhttp.onreadystatechange=onStateChange;

		xmlhttp.open("GET", Url, true);

		//console.log("send");                                                   //  (httpMethod,  URL,  asynchronous)

		// xmlhttp.overrideMimeType('text/xml');

		xmlhttp.send(null);
	}
	else
	{
		alert("The XMLHttpRequest not supported");
	}
}

function convert(str)
{
	var xmlStr = str.replace('<','<');
	xmlStr = xmlStr.replace('>','>');
	xmlStr = xmlStr.replace('"','"');
	xmlStr = xmlStr.replace('&',"&");
	return xmlStr;
}

function onStateChange()  {

	if (xmlhttp.readyState == 4) {

	   if (xmlhttp.status == 200) {

			var xml = xmlhttp.responseXML;

			console.log("xml " + xml);

			if(xml)
			{
				var markers = xml.documentElement.getElementsByTagName("marker");

				var html = "";
				var newMessage = false;

				//console.log("count: "+markers.length);

				//console.log("markers: "+markers);

				for (var i = 0; i < markers.length; i++)
				{
					var count = markers[i].getAttribute("count");

					document.getElementById("processed").innerHTML = count;
				}
			}
			else
			{
				//console.log("no new chats");
			}

		} else {

				alert("statusText: " + xmlhttp.statusText + "\nHTTP status code: " + xmlhttp.status);

		}  // End of:   if (xmlhttp.status==200)
  }
}
