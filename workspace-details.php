<?php

require_once( 'watsonscraper.php' );
include( "functions.php" );

$colname_rsWorkspaceDetails = "-1";
if ( isset( $_GET[ 'wid' ] ) ) {
  $colname_rsWorkspaceDetails = $_GET[ 'wid' ];
}
mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsWorkspaceDetails = sprintf( "SELECT * FROM workspaces WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsWorkspaceDetails = mysql_query( $query_rsWorkspaceDetails, $watsonscraper )or die( mysql_error() );
$row_rsWorkspaceDetails = mysql_fetch_assoc( $rsWorkspaceDetails );
$totalRows_rsWorkspaceDetails = mysql_num_rows( $rsWorkspaceDetails );

mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsRecommendationInfo = sprintf( "SELECT * FROM recommendations WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsRecommendationInfo = mysql_query( $query_rsRecommendationInfo, $watsonscraper )or die( mysql_error() );
$row_rsRecommendationInfo = mysql_fetch_assoc( $rsRecommendationInfo );
$totalRows_rsRecommendationInfo = mysql_num_rows( $rsRecommendationInfo );

mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsIntentInfo = sprintf( "SELECT * FROM intents WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsIntentInfo = mysql_query( $query_rsIntentInfo, $watsonscraper )or die( mysql_error() );
$row_rsIntentInfo = mysql_fetch_assoc( $rsIntentInfo );
$totalRows_rsIntentInfo = mysql_num_rows( $rsIntentInfo );

mysql_select_db( $database_watsonscraper, $watsonscraper );
$query_rsLogInfo = sprintf( "SELECT * FROM logs WHERE workspaceid = %s", GetSQLValueString( $colname_rsWorkspaceDetails, "int" ) );
$rsLogInfo = mysql_query( $query_rsLogInfo, $watsonscraper )or die( mysql_error() );
$row_rsLogInfo = mysql_fetch_assoc( $rsLogInfo );
$totalRows_rsLogInfo = mysql_num_rows( $rsLogInfo );

//echo $query_rsRecommendationInfo;

?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Workspace Details - <?php echo $row_rsWorkspaceDetails['workspacename']; ?></title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Workspace Details</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p id="workspaceLbl"><?php echo $row_rsWorkspaceDetails['workspacename']; ?></p>
    <div id="renderContent">
      <table width="100%" cellspacing="5">
        <tbody>
          <?php if($totalRows_rsIntentInfo > 0) { ?>
          <tr>
            <td><a href="view-intents.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Watson Intents</a></td>
          </tr>
          <?php } ?>
          <tr>
            <td><a href="parse-logs-file.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Upload Logs</a></td>
          </tr>
            
          <?php if($totalRows_rsLogInfo > 0) { ?>
          <tr>
            <td><a href="view-logs.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Logs</a></td>
          </tr>
                      <?php } ?>
                                  <?php if($totalRows_rsRecommendationInfo > 0) { ?>


          <tr>
            <td><a href="intent-recommendation-list.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Intent Recommendation Results</a></td>
          </tr>
            

          <tr>
            <td><a href="export-results.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Export Results</a></td>
          </tr>
            
                      <?php } ?>
          <?php if($totalRows_rsLogInfo > 0) { ?>

            
          <tr>
            <td><a href="create-watson-file.php?wid=<?php echo $row_rsWorkspaceDetails['workspaceid']; ?>">Create Intent Recommendation File</a></td>
          </tr>
            
                                  <?php } ?>

          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
    <p id="docContentDiv"> </p>
  </div>
  <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>
<?php
mysql_free_result($rsWorkspaceDetails);

?>