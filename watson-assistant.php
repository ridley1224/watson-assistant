<?php
require_once( 'watsonscraper.php' );
include( "functions.php" );

if ( isset( $_POST[ 'apikey' ] ) ) {

  if ( strlen( $_POST[ 'apikey' ] ) < 1 ) {

    $status = "Please enter your api key.";
  } else {

    $str = "apikey:" . $_POST[ 'apikey' ];

    $en = base64_encode( $str );

    $curl = curl_init();

    curl_setopt_array( $curl, array(
      CURLOPT_URL => "https://gateway.watsonplatform.net/assistant/api/v1/workspaces?version=2019-02-28",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic {$en}",
        "Postman-Token: ce0ac21e-2f91-4ec2-96fc-27a4805cbe57",
        "cache-control: no-cache"
      ),
    ) );

    $response = curl_exec( $curl );
    $err = curl_error( $curl );

    curl_close( $curl );

    if ( $err ) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;

      $json = json_decode( $response );

      if ( $json->workspaces ) {

        $workspaces = $json->workspaces;

        foreach ( $workspaces as $ws ) {
          //echo $ws->name . "<br>";

          mysql_select_db( $database_watsonscraper, $watsonscraper );

          $wid = $ws->workspace_id;
          $wname = $ws->name;

          $query_rsRecommendationInfo = sprintf( "SELECT * FROM workspaces WHERE watsonworkspaceid = %s AND workspacename = %s",
            GetSQLValueString( $wid, "text" ),
            GetSQLValueString( $wname, "int" ) );

          $rsRecommendationInfo = mysql_query( $query_rsRecommendationInfo, $watsonscraper )or die( mysql_error() );
          $row_rsRecommendationInfo = mysql_fetch_assoc( $rsRecommendationInfo );
          $totalRows_rsRecommendationInfo = mysql_num_rows( $rsRecommendationInfo );

          if ( $totalRows_rsRecommendationInfo == 0 ) {

            $insertSQL = sprintf( "INSERT INTO workspaces (workspacename, watsonworkspaceid, userid,datecreated) VALUES (%s,%s,%s,%s)",
              GetSQLValueString( $ws->name, "text" ),
              GetSQLValueString( $ws->workspace_id, "text" ),
              GetSQLValueString( $_SESSION[ 'uid' ], "int" ),
              GetSQLValueString( $date, "date" ) );

            mysql_select_db( $database_watsonscraper, $watsonscraper );
            $Result1 = mysql_query( $insertSQL, $watsonscraper )or die( mysql_error() );
            $last_id = mysql_insert_id();

            //get workspace intents

            $curl2 = curl_init();

            curl_setopt_array( $curl2, array(
              CURLOPT_URL => "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/{$ws->workspace_id}/intents?version=2019-02-28",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "Authorization: Basic {$en}",
                "Cache-Control: no-cache",
                "Connection: keep-alive"
              ),
            ) );

            $response2 = curl_exec( $curl2 );
            $err2 = curl_error( $curl2 );

            curl_close( $curl );

            if ( $err ) {
              echo "cURL Error #:" . $err2;
            } else {
              //echo $response2;

              $json2 = json_decode( $response2 );

              if ( $json2->intents ) {

                $intents = $json2->intents;

                foreach ( $intents as $intent ) {

                  $query_rsIntentInfo = sprintf( "SELECT * FROM intents WHERE workspaceid = %s AND intent = %s",
                    GetSQLValueString( $last_id, "text" ),
                    GetSQLValueString( $intent->intent, "text" ) );

                  $rsIntentInfo = mysql_query( $query_rsIntentInfo, $watsonscraper )or die( mysql_error() );
                  $row_rsIntentInfo = mysql_fetch_assoc( $rsIntentInfo );
                  $totalRows_rsIntentInfo = mysql_num_rows( $rsIntentInfo );

                  if ( $totalRows_rsIntentInfo == 0 ) {

                    $insertSQL2 = sprintf( "INSERT INTO intents (intent, description, workspaceid, datecreated) VALUES (%s,%s,%s,%s)",
                      GetSQLValueString( $intent->intent, "text" ),
                      GetSQLValueString( $intent->description, "text" ),
                      GetSQLValueString( $last_id, "text" ),
                      GetSQLValueString( $date, "date" ) );

                    mysql_select_db( $database_watsonscraper, $watsonscraper );
                    $Result2 = mysql_query( $insertSQL2, $watsonscraper )or die( mysql_error() );
                    $last_id2 = mysql_insert_id();
                  }
                }
              }
            }
          }
        }

        //navigate to my-workspaces

        $MM_redirectLoginSuccess = "my-workspaces.php";
        header( "Location: " . $MM_redirectLoginSuccess );

      } else {

        $status = "Unauthorized api key.";
      }
    }
  }
} else if ( isset( $_POST[ 'workspace' ] ) ) {

  if ( strlen( $_POST[ 'workspace' ] ) > 0 ) {
    $query_rsWorkspaceInfo = sprintf( "SELECT * FROM workspaces WHERE watsonworkspaceid = %s AND workspacename = %s",
      GetSQLValueString( $wid, "text" ),
      GetSQLValueString( $wname, "int" ) );

    $rsWorkspaceInfo = mysql_query( $query_rsWorkspaceInfo, $watsonscraper )or die( mysql_error() );
    $row_rsWorkspaceInfo = mysql_fetch_assoc( $rsWorkspaceInfo );
    $totalRows_rsWorkspaceInfo = mysql_num_rows( $rsWorkspaceInfo );

    if ( $totalRows_rsWorkspaceInfo == 0 ) {

      $insertSQL = sprintf( "INSERT INTO workspaces (workspacename, userid,datecreated) VALUES (%s,%s,%s)",
        GetSQLValueString( trim( $_POST[ 'workspace' ] ), "text" ),
        GetSQLValueString( $_SESSION[ 'uid' ], "int" ),
        GetSQLValueString( $date, "date" ) );

      mysql_select_db( $database_watsonscraper, $watsonscraper );
      $Result1 = mysql_query( $insertSQL, $watsonscraper )or die( mysql_error() );
      $last_id = mysql_insert_id();

      $MM_redirectLoginSuccess = "my-workspaces.php";
      header( "Location: " . $MM_redirectLoginSuccess );
    }
  } else {
    $status1 = "Please enter a workspace name.";
  }
}

?>

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<script src="js/jquery-1.11.1.min.js"></script> 
<script src="js/side-nav.js"></script>
<title>Create Workspace</title>
</head>
<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <?php include("includes/header.php"); ?>
  <?php include("includes/nav.php");?>
  <div id="titleDiv" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Create Workspace</p>
    </div>
  </div>
  <div id="contentBG" class="clearfix">
    <p id="workspaceLbl"><?php echo $row_rsWorkspaceDetails['workspacename']; ?></p>
    <div id="renderContent">
      <form action="<?php echo $loginFormAction; ?>" id="form1" name="form1" method="POST">
        <table width="600px" cellpadding="5" cellspacing="5">
          <tbody>
            <tr>
              <td width="12%">Workspace name</td>
              <td width="95%"><input name="workspace" type="text" id="workspace" value="<?php if(isset($_POST['workspace'])) {echo $_POST['workspace'];} ?>" class="inputBox" ></td>
            </tr>
            <?php if(isset($status1)){ ?>
            <tr>
              <td>&nbsp;</td>
              <td><span class="status"><?php echo $status1;?></span></td>
            </tr>
            <?php } ?>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="submit" id="submit" value="Submit" class="submitBtn"></td>
            </tr>
          </tbody>
        </table>
        <input type="hidden" name="MM_update2" value="form1">
  </form>
      <br>
      <p style="margin-bottom: 15px;">You can import your Watson Assistant workspace data. For security, we do not store your api key.</p>
      <form action="<?php echo $loginFormAction; ?>" id="form2" name="form2" method="POST">
        <table width="600px" cellpadding="5" cellspacing="5">
          <tbody>
            <tr>
              <td width="12%">API Key</td>
              <td width="95%"><input name="apikey" type="password" id="apikey" value="<?php if(isset($_POST['apikey'])) {echo $_POST['apikey'];} ?>"  class="inputBox"></td>
            </tr>
            <?php if(isset($status)){ ?>
            <tr>
              <td>&nbsp;</td>
              <td><span class="status"><?php echo $status;?></span></td>
            </tr>
            <?php } ?>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="submit1" id="submit1" value="Submit" class="submitBtn"></td>
            </tr>
          </tbody>
        </table>
        <input type="hidden" name="MM_update2" value="form2">
  </form>
    </div>
  </div>
  <input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['uid']; ?>">
</div>
<?php include("includes/side-nav.php");?>
</body>
</html>