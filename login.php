<?php

require_once( 'watsonscraper.php' );
include( "functions.php" );

$loginFormAction = $_SERVER[ 'PHP_SELF' ];
if ( isset( $_GET[ 'accesscheck' ] ) ) {
  $_SESSION[ 'PrevUrl' ] = $_GET[ 'accesscheck' ];
}

if ( isset( $_POST[ 'username' ] ) ) {
  $loginUsername = $_POST[ 'username' ];
  $password = $_POST[ 'password' ];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "my-workspaces.php";
  $MM_redirectLoginFailed = "login.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db( $database_watsonscraper, $watsonscraper );

  $LoginRS__query = sprintf( "SELECT email, password, userid FROM users WHERE email=%s AND password=%s",
    GetSQLValueString( $loginUsername, "text" ), GetSQLValueString( $password, "text" ) );

  $LoginRS = mysql_query( $LoginRS__query, $watsonscraper )or die( mysql_error() );
  $row_rsUserInfo = mysql_fetch_assoc( $LoginRS );
  $loginFoundUser = mysql_num_rows( $LoginRS );
  if ( $loginFoundUser ) {
    $loginStrGroup = "";

    if ( PHP_VERSION >= 5.1 ) {
      session_regenerate_id( true );
    } else {
      session_regenerate_id();
    }
    //declare two session variables and assign them
    $_SESSION[ 'MM_Username' ] = $loginUsername;
    $_SESSION[ 'MM_UserGroup' ] = $loginStrGroup;
    $_SESSION[ 'uid' ] = $row_rsUserInfo[ 'userid' ];

    if ( isset( $_SESSION[ 'PrevUrl' ] ) && false ) {
      $MM_redirectLoginSuccess = $_SESSION[ 'PrevUrl' ];
    }
    header( "Location: " . $MM_redirectLoginSuccess );
  } else {
    header( "Location: " . $MM_redirectLoginFailed . "?failed=true" );
  }
}
?>
<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/boilerplate.css">
<link rel="stylesheet" href="css/my-files.css">
<script src="jquery/jquery-1.11.1.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>Login</title>
</head>

<body>
<div id="primaryContainer" class="primaryContainer clearfix">
  <div id="headerBG" class="clearfix"> <a href="index.php"><img id="logo" src="img/wa48.png" class="image"/></a> </div>
  <div id="titleDiv2" class="clearfix">
    <div id="headerTxtBG" class="clearfix">
      <p id="headerLbl">Login</p>
    </div>
  </div>
  <div id="contentBG2" class="clearfix">
    <form action="<?php echo $loginFormAction; ?>" id="form1" name="form1" method="POST">
      <p>&nbsp;</p>
      <table width="100%" cellpadding="5" cellspacing="5">
        <tbody>
          <tr>
            <td width="12%">Email</td>
            <td width="88%"><input name="username" type="text" id="username" value="<?php if(isset($_POST['username'])) {echo $_POST['username'];} ?>" class="inputBox"></td>
          </tr>
          <tr>
            <td>Password</td>
            <td><input name="password" type="password" id="password" value="" class="inputBox"></td>
          </tr>
            <?php if(isset($_GET['failed'])){ ?>
            <tr>
              <td>&nbsp;</td>
              <td><span class="status" style="margin-left: 30px">Login unsuccessful</span></td>
            </tr>
            <?php } ?>
          <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" id="submit" value="Submit" class="submitBtn2"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a href="create-account.php"><span style="margin-left: 40px;">Create Account</span></a></td>
          </tr>
        </tbody>
      </table>
      <input type="hidden" name="MM_update" value="form1">
</form>
    <p id="docContentDiv">&nbsp;</p>
  </div>
</div>
</body>
</html>