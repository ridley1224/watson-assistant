<?php

$currentPage = $_SERVER[ "PHP_SELF" ];

$maxRows_rsUttterances = 25;
$pageNum_rsUttterances = 0;
if ( isset( $_GET[ 'pageNum_rsUttterances' ] ) ) {
    $pageNum_rsUttterances = $_GET[ 'pageNum_rsUttterances' ];
}
$startRow_rsUttterances = $pageNum_rsUttterances * $maxRows_rsUttterances;

mysql_select_db( $database_watsonscraper, $watsonscraper );

$filter = " WHERE recid = {$_GET[ 'recid' ]} AND utterance IS NOT NULL";

if ( isset( $_POST[ 'search' ] ) || isset( $_GET[ 'search' ] ) ) {

    if ( isset( $_POST[ 'search' ] ) && strlen($_POST[ 'search' ]) > 0) {

        $search = $_POST[ 'search' ];
    }
    else if ( isset( $_GET[ 'search' ] ) && strlen($_GET[ 'search' ]) > 0) {
    
        $search = $_GET[ 'search' ];
    }

    if(strlen($search))
    {
        $filter .= " AND utterance LIKE '%{$search}%'";
    }

} else {

    //$filter = "";;
}

$query_rsUttterances = "SELECT * FROM recomendationutterances {$filter}";

//echo "query: " . $query_rsUttterances;

    $rsUttterances = mysql_query( $query_rsUttterances, $watsonscraper )or die( mysql_error() );
    $row_rsUttterances = mysql_fetch_assoc( $rsUttterances );
    $totalRows_rsUttterances = mysql_num_rows( $rsUttterances );

$query_limit_rsUttterances = sprintf( "%s LIMIT %d, %d", $query_rsUttterances, $startRow_rsUttterances, $maxRows_rsUttterances );

//echo "query: " . $query_limit_rsUttterances;

$rsUttterances = mysql_query( $query_limit_rsUttterances, $watsonscraper )or die( mysql_error() );
$row_rsUttterances = mysql_fetch_assoc( $rsUttterances );

if ( isset( $_GET[ 'totalRows_rsUttterances' ] ) ) {
    $totalRows_rsUttterances = $_GET[ 'totalRows_rsUttterances' ];
} else {
    $all_rsUttterances = mysql_query( $query_rsUttterances );
    $totalRows_rsUttterances = mysql_num_rows( $all_rsUttterances );
}
$totalPages_rsUttterances = ceil( $totalRows_rsUttterances / $maxRows_rsUttterances ) - 1;

$queryString_rsUttterances = "";
if ( !empty( $_SERVER[ 'QUERY_STRING' ] ) ) {
    $params = explode( "&", $_SERVER[ 'QUERY_STRING' ] );
    $newParams = array();
    foreach ( $params as $param ) {
        if ( stristr( $param, "pageNum_rsUttterances" ) == false &&
            stristr( $param, "totalRows_rsUttterances" ) == false ) {
            array_push( $newParams, $param );
        }
    }
    if ( count( $newParams ) != 0 ) {
        $queryString_rsUttterances = "&" . htmlentities( implode( "&", $newParams ) );
    }
}
$queryString_rsUttterances = sprintf( "&totalRows_rsUttterances=%d%s", $totalRows_rsUttterances, $queryString_rsUttterances );

if ( $totalRows_rsUttterances > 0 ) {
    ?>
    <table width="100%" cellpadding="5" cellspacing="5" class="contentTable">
        <tbody>
            <tr>
                <td width="17%"><strong>Utterance</strong>
                </td>
                <td width="22%"><strong>Occurences</strong>
                </td>
            </tr>

            <?php do { ?>
            <tr>
                <td class="hr">
                    <?php

                    echo $row_rsUttterances[ 'utterance' ] ;
                    ?>
                </td>
                <td>
                    <?php echo $row_rsUttterances[ 'occurrences' ]?>
                </td>
            </tr>

            <?php } while ($row_rsUttterances = mysql_fetch_assoc($rsUttterances));  ?>
        </tbody>
    </table>

<?php

    if(isset($_GET['pageNum_rsIntents']))
    {
        $page = "&pageNum_rsIntents={$_GET['pageNum_rsIntents']}";
    }

    if(isset($_GET['search']))
    {
        if(isset($_GET['pageNum_rsIntents']))
        {
            $searchP = "&search={$_GET['search']}";
        }
        else
        {
            $searchP = "?search={$_GET['search']}";
        }
    }
    
    //$widP = "&wid={$colname_rsWorkspaceDetails}";

    $params = $page . $searchP;

    ?>

    <p style="margin-top: 20px;"> 
        <?php if ($pageNum_rsUttterances > 0) { // Show if not first page ?>
        <a href="<?php printf(" %s?pageNum_rsUttterances=%d%s ", $currentPage, 0, $queryString_rsUttterances); ?>">First</a>
        <?php } // Show if not first page ?>
        <?php if ($pageNum_rsUttterances > 0) { // Show if not first page ?>
        <a href="<?php printf(" %s?pageNum_rsUttterances=%d%s ", $currentPage, max(0, $pageNum_rsUttterances - 1), $queryString_rsUttterances); ?>">Previous</a>
        <?php } // Show if not first page ?>
        <?php if ($pageNum_rsUttterances < $totalPages_rsUttterances) { // Show if not last page ?>
        <a href="<?php printf(" %s?pageNum_rsUttterances=%d%s ", $currentPage, min($totalPages_rsUttterances, $pageNum_rsUttterances + 1), $queryString_rsUttterances); ?>">Next</a>
        <?php } // Show if not last page ?>
        <?php if ($pageNum_rsUttterances < $totalPages_rsUttterances) { // Show if not last page ?>
        <a href="<?php printf(" %s?pageNum_rsUttterances=%d%s ", $currentPage, $totalPages_rsUttterances, $queryString_rsUttterances); ?>">Last</a>
        <?php } // Show if not last page ?>
    </p>

    <p style="margin-top: 10px;">Showing <?php echo ($startRow_rsUttterances + 1) ?>-<?php echo min($startRow_rsUttterances + $maxRows_rsUttterances, $totalRows_rsUttterances) ?> of <?php echo $totalRows_rsUttterances ?></p>

    
<br>
    <a href='intent-recommendation-list.php?wid=<?php echo $_GET['wid']; echo $params;?>'>Back</a>
    <?php } else { echo "You currently don't have any utterances that match <strong>{$_GET[ 'intent' ]}</strong>."; }?>