<?php

//parse chat logs csv file and saves lines into mysql database

//$_FILES[ 'file' ] = true;
include( "../functions.php" );


if ( isset( $_FILES[ 'file' ] ) ) {

  date_default_timezone_set( 'America/Detroit' );
  $date = date( "Y-m-d H:i:s" );

  //echo "Started processing at " . $date . "<br>";

  //$csvFile = $_FILES["upload"]["tmp_name"];
  $csvFile = $_FILES[ 'file' ][ 'tmp_name' ];

  //$csvFile = "../includes/userlogs2.csv";
  //$csvFile = fopen($path, 'r');


  //var_dump($csvFile);

  $obj = getCSVColumns( $csvFile );

  $csvColumnCount = $obj->cols;

  $colNames = $obj->colNames;

  //echo "csvColumnCount: {$csvColumnCount}<br>";

  $utteranceList = readCSV( $csvFile );

  //add UI to ask for column names and populate here

  $user_CSV = array( 'date', 'timestamp', 'id', 'user', 'input', 'intent', 'confidence', 'month' );

  //select count(id) as 'total' FROM logs WHERE input is NULL

  //echo "output:<br>";

  //     print "<pre>";
  //     print_r( $csvList );
  //     print "</pre>";

  //echo "cols: {$csvColumnCount}";
  echo json_encode( $obj );
}

?>