<?php

$currentPage = $_SERVER[ "PHP_SELF" ];

$maxRows_rsIntents = 25;
$pageNum_rsIntents = 0;
if ( isset( $_GET[ 'pageNum_rsIntents' ] ) ) {
    $pageNum_rsIntents = $_GET[ 'pageNum_rsIntents' ];
}
$startRow_rsIntents = $pageNum_rsIntents * $maxRows_rsIntents;

mysql_select_db( $database_watsonscraper, $watsonscraper );

if ( isset( $_POST[ 'search' ] ) || isset( $_GET[ 'search' ] ) ) {

    if ( isset( $_POST[ 'search' ] ) && strlen($_POST[ 'search' ]) > 0) {

        $search = $_POST[ 'search' ];
    }
    else if ( isset( $_GET[ 'search' ] ) && strlen($_GET[ 'search' ]) > 0) {
    
        $search = $_GET[ 'search' ];
    }

    if(strlen($search))
    {
        $filter = " AND recommendation LIKE '%{$search}%'";
    }

} else {

    $filter = "";;
}

$query_rsIntents = "SELECT * FROM recommendations WHERE workspaceid = {$colname_rsWorkspaceDetails} {$filter}";

//echo "query: " . $query_rsIntents;
$query_limit_rsIntents = sprintf( "%s LIMIT %d, %d", $query_rsIntents, $startRow_rsIntents, $maxRows_rsIntents );
$rsIntents = mysql_query( $query_limit_rsIntents, $watsonscraper )or die( mysql_error() );
$row_rsIntents = mysql_fetch_assoc( $rsIntents );
//$totalRows_rsIntents = mysql_num_rows( $rsIntents );

//echo "<br> limit query: " . $query_limit_rsIntents;

if ( isset( $_GET[ 'totalRows_rsIntents' ] ) ) {
    $totalRows_rsIntents = $_GET[ 'totalRows_rsIntents' ];
} else {
    $all_rsIntents = mysql_query( $query_rsIntents );
    $totalRows_rsIntents = mysql_num_rows( $all_rsIntents );
}
$totalPages_rsIntents = ceil( $totalRows_rsIntents / $maxRows_rsIntents ) - 1;

$queryString_rsIntents = "";
if ( !empty( $_SERVER[ 'QUERY_STRING' ] ) ) {
    $params = explode( "&", $_SERVER[ 'QUERY_STRING' ] );
    $newParams = array();
    foreach ( $params as $param ) {
        if ( stristr( $param, "pageNum_rsIntents" ) == false &&
            stristr( $param, "totalRows_rsIntents" ) == false ) {
            array_push( $newParams, $param );
        }
    }
    if ( count( $newParams ) != 0 ) {
        $queryString_rsIntents = "&" . htmlentities( implode( "&", $newParams ) );
    }
}
$queryString_rsIntents = sprintf( "&totalRows_rsIntents=%d%s", $totalRows_rsIntents, $queryString_rsIntents );


if ( $totalRows_rsIntents > 0 ) {

    //echo "<br><br>matched records: {$totalRows_rsIntents}";

    ?>
    <table width="100%" cellpadding="5" cellspacing="5" class="contentTable">
        <tbody>
            <tr>
                <td width="17%"><strong>Intent</strong>
                </td>
                <td width="22%"><strong>Utterances</strong>
                </td>
            </tr>

            <?php do { 
                
                $query_rsCount = "SELECT count(uid) as 'count' FROM recomendationutterances WHERE recid = {$row_rsIntents[ 'recid' ]}";

                $rsCount = mysql_query( $query_rsCount, $watsonscraper )or die( mysql_error() );
                $row_rsCount = mysql_fetch_assoc( $rsCount );
                $totalRows_rsCount = mysql_num_rows( $rsCount );
                                
                ?>
            <tr>
                <td class="hr">
                    <?php

                    if(isset($_GET['pageNum_rsIntents']))
                    {
                        $page = "&pageNum_rsIntents={$_GET['pageNum_rsIntents']}";
                    }

                    if(strlen($search) > 0)
                    {
                        $searchP = "&search={$search}";
                    }
        
        $widP = "&wid={$colname_rsWorkspaceDetails}";
        

                    echo "<a href='intent-details.php?wid={$colname_rsWorkspaceDetails}&recid={$row_rsIntents[ 'recid' ]}&intent={$row_rsIntents[ 'recommendation' ]}{$page}{$searchP}'>{$row_rsIntents[ 'recommendation' ]}</a>" ;
                    ?>
                </td>
                <td>
                    <?php echo $row_rsCount['count']; ?>
                </td>
            </tr>

            <?php } while ($row_rsIntents = mysql_fetch_assoc($rsIntents));  ?>
        </tbody>
    </table>



    <p style="margin-top: 20px;">
        <?php if ($pageNum_rsIntents > 0) { // Show if not first page ?>
        <a href="<?php printf(" %s?pageNum_rsIntents=%d%s%s%s  ", $currentPage, 0, $queryString_rsIntents, $searchP, $widP); ?>">First</a>
        <?php } // Show if not first page ?>
        <?php if ($pageNum_rsIntents > 0) { // Show if not first page ?>
        <a href="<?php printf(" %s?pageNum_rsIntents=%d%s%s%s  ", $currentPage, max(0, $pageNum_rsIntents - 1), $queryString_rsIntents, $searchP, $widP); ?>">Previous</a>
        <?php } // Show if not first page ?>
        <?php if ($pageNum_rsIntents < $totalPages_rsIntents) { // Show if not last page ?>
        <a href="<?php printf(" %s?pageNum_rsIntents=%d%s%s%s  ", $currentPage, min($totalPages_rsIntents, $pageNum_rsIntents + 1), $queryString_rsIntents, $searchP, $widP); ?>">Next</a>
        <?php } // Show if not last page ?>
        <?php if ($pageNum_rsIntents < $totalPages_rsIntents) { // Show if not last page ?>
        <a href="<?php printf(" %s?pageNum_rsIntents=%d%s%s%s  ", $currentPage, $totalPages_rsIntents, $queryString_rsIntents, $searchP, $widP); ?>">Last</a>
        <?php } // Show if not last page ?>
    </p>

    <p style="margin-top: 10px;">Showing <?php echo ($startRow_rsIntents + 1) ?>-<?php echo min($startRow_rsIntents + $maxRows_rsIntents, $totalRows_rsIntents) ?> of <?php echo $totalRows_rsIntents ?></p>

    <?php } else { 
    
    if(isset($search))
    {
        echo "You currently don't have any intent recommendations that match <strong>" . $search . "</strong>."; 
    }
    else
    {
        echo "You currently don't intent recommendations. You can upload intent recommendation file <a href=\"create-watson-file.php?wid={$row_rsWorkspaceDetails['workspaceid']}\">here</a>."; 
    }
    }?>