<?php

$currentPage = $_SERVER[ "PHP_SELF" ];

$maxRows_rsLogs = 25;
$pageNum_rsLogs = 0;
if ( isset( $_GET[ 'pageNum_rsLogs' ] ) ) {
  $pageNum_rsLogs = $_GET[ 'pageNum_rsLogs' ];
}
$startRow_rsLogs = $pageNum_rsLogs * $maxRows_rsLogs;

mysql_select_db( $database_watsonscraper, $watsonscraper );

if ( isset( $_POST[ 'search' ] ) || isset( $_GET[ 'search' ] ) ) {

  if ( isset( $_POST[ 'search' ] ) && strlen( $_POST[ 'search' ] ) > 0 ) {

    $search = $_POST[ 'search' ];
  } else if ( isset( $_GET[ 'search' ] ) && strlen( $_GET[ 'search' ] ) > 0 ) {

    $search = $_GET[ 'search' ];
  }

  if ( strlen( $search ) ) {
    $filter = " AND workspacename LIKE '%{$search}%'";
  }
}

$query_rsLogs = "SELECT * FROM workspaces WHERE 0=0 AND userid = {$_SESSION['uid']} {$filter} ";

//echo "query: " . $query_rsLogs;

$rsLogs = mysql_query( $query_rsLogs, $watsonscraper )or die( mysql_error() );
$row_rsLogs = mysql_fetch_assoc( $rsLogs );
$totalRows_rsLogs = mysql_num_rows( $rsLogs );

$query_limit_rsLogs = sprintf( "%s LIMIT %d, %d", $query_rsLogs, $startRow_rsLogs, $maxRows_rsLogs );

//echo "query: " . $query_limit_rsLogs;

$rsLogs = mysql_query( $query_limit_rsLogs, $watsonscraper )or die( mysql_error() );
$row_rsLogs = mysql_fetch_assoc( $rsLogs );

if ( isset( $_GET[ 'totalRows_rsLogs' ] ) ) {
  $totalRows_rsLogs = $_GET[ 'totalRows_rsLogs' ];
} else {
  $all_rsLogs = mysql_query( $query_rsLogs );
  $totalRows_rsLogs = mysql_num_rows( $all_rsLogs );
}
$totalPages_rsLogs = ceil( $totalRows_rsLogs / $maxRows_rsLogs ) - 1;

$queryString_rsLogs = "";
if ( !empty( $_SERVER[ 'QUERY_STRING' ] ) ) {
  $params = explode( "&", $_SERVER[ 'QUERY_STRING' ] );
  $newParams = array();
  foreach ( $params as $param ) {
    if ( stristr( $param, "pageNum_rsLogs" ) == false &&
      stristr( $param, "totalRows_rsLogs" ) == false ) {
      array_push( $newParams, $param );
    }
  }
  if ( count( $newParams ) != 0 ) {
    $queryString_rsLogs = "&" . htmlentities( implode( "&", $newParams ) );
  }
}

$queryString_rsLogs = sprintf( "&totalRows_rsLogs=%d%s", $totalRows_rsLogs, $queryString_rsLogs );

if ( $totalRows_rsLogs > 0 ) {
  ?>
<table width="100%" cellpadding="5" cellspacing="5" class="contentTable">
  <tbody>
    <tr>
      <td width="27%"><strong>Workspace</strong></td>
      <td width="73%">&nbsp;</td>
    </tr>
    <?php do { ?>
      <tr>
        <td class="hr"><a href="workspace-details.php?wid=<?php echo $row_rsLogs[ 'workspaceid' ] ; ?>"><?php echo $row_rsLogs[ 'workspacename' ] ; ?></a></td>
        <td class="hr"><?php if($row_rsLogs[ 'watsonworkspaceid' ]){ ?> <img src="img/wa16.png"><?php }?></td>
      </tr>
      <?php } while ($row_rsLogs = mysql_fetch_assoc($rsLogs));  ?>
  </tbody>
</table>
<?php

if(isset($_GET['pageNum_rsIntents']))
{
    $page = "?pageNum_rsIntents={$_GET['pageNum_rsIntents']}";
}

if(isset($search))
{
    $searchP = "&search={$search}";
}

$widP = "&wid={$colname_rsWorkspaceDetails}";
    
?>
<p style="margin-top: 20px;">
  <?php if ($pageNum_rsLogs > 0) { // Show if not first page ?>
    <a href="<?php printf(" %s?pageNum_rsLogs=%d%s%s%s  ", $currentPage, 0, $queryString_rsLogs, $searchP, $widP); ?>">First</a>
    <?php } // Show if not first page ?>
  <?php if ($pageNum_rsLogs > 0) { // Show if not first page ?>
    <a href="<?php printf(" %s?pageNum_rsLogs=%d%s%s%s  ", $currentPage, max(0, $pageNum_rsLogs - 1), $queryString_rsLogs, $searchP, $widP); ?>">Previous</a>
    <?php } // Show if not first page ?>
  <?php if ($pageNum_rsLogs < $totalPages_rsLogs) { // Show if not last page ?>
    <a href="<?php printf(" %s?pageNum_rsLogs=%d%s%s%s  ", $currentPage, min($totalPages_rsLogs, $pageNum_rsLogs + 1), $queryString_rsLogs, $searchP, $widP); ?>">Next</a>
    <?php } // Show if not last page ?>
  <?php if ($pageNum_rsLogs < $totalPages_rsLogs) { // Show if not last page ?>
    <a href="<?php printf(" %s?pageNum_rsLogs=%d%s%s%s  ", $currentPage, $totalPages_rsLogs, $queryString_rsLogs, $searchP, $widP); ?>">Last</a>
    <?php } // Show if not last page ?>
</p>
<p style="margin-top: 10px;">Showing <?php echo ($startRow_rsLogs + 1) ?>-<?php echo min($startRow_rsLogs + $maxRows_rsLogs, $totalRows_rsLogs) ?> of <?php echo $totalRows_rsLogs ?></p>
<br>
<?php } else {

if(isset($search))
{
echo "You currently don't have any workspaces that match <strong>" . $search . "</strong>."; 
}
else
{
echo "You currently don't have any workspaces. You can create a workspace <a href=\"watson-assistant.php\">here</a>."; 
}

}?>
